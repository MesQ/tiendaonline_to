﻿<%@ Page Title="Inicio" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TiendaOnline.Web._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
    .carousel{
        background: #2f4357;
        margin-top: 0px;
    }
    .carousel .item{
        min-height: 500px; /* Prevent carousel from being distorted if for some reason image doesn't load */
    }
    .carousel .item img{
        margin: 0 auto; /* Align slide image horizontally center */
    }
    .bs-example{
	    margin: 0px;
    }
    </style>

    <div class="bs-example">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Carousel indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>   
        <!-- Wrapper for carousel items -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="/images/img1.jpg" alt="First Slide">
            </div>
            <div class="item">
                <img src="/images/img2.jpeg" alt="Second Slide">
            </div>
            <div class="item">
                <img src="/images/img3.jpg" alt="Third Slide">
            </div>
        </div>
        <!-- Carousel controls -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div><br /> <br />
</div>
    <div class="form-horizontal">
        <div align="center">
    <div class="form-group">
        <div class="col-md-4">
            <h4>Móviles</h4>
            <asp:ImageButton ID="ImageButton1" runat="server" CssClass="btn btn-default" ImageUrl="~/Images/smartphone.png" PostBackUrl="~/Products.aspx?id=1" />
            
        </div>
        <div class="col-md-4">
            <h4>Portátiles</h4>
            <asp:ImageButton ID="ImageButton2" runat="server" CssClass="btn btn-default" ImageUrl="~/Images/laptop.png" PostBackUrl="~/Products.aspx?id=4" />
        </div>
        <div class="col-md-4">
            <h4>Sobremesas</h4>
            <asp:ImageButton ID="ImageButton3" runat="server" CssClass="btn btn-default" ImageUrl="~/Images/gaming.png" PostBackUrl="~/Products.aspx?id=5" />
        </div>
    </div>
    

    <div class="form-group">
        <div class="col-md-4">
            <h4>Tablets</h4>
            <asp:ImageButton ID="ImageButton4" runat="server" CssClass="btn btn-default" ImageUrl="~/Images/tablet.png" PostBackUrl="~/Products.aspx?id=3" />
        </div>
        <div class="col-md-4">
            <h4>Monitores</h4>
            <asp:ImageButton ID="ImageButton5" runat="server" CssClass="btn btn-default" ImageUrl="~/Images/coding.png" PostBackUrl="~/Products.aspx?id=6" />
        </div>
        <div class="col-md-4">
            <h4>Periféricos</h4>
            <asp:ImageButton ID="ImageButton6" runat="server" CssClass="btn btn-default" ImageUrl="~/Images/type.png" PostBackUrl="~/Products.aspx?id=7"/>
        </div>
    </div>
    </div>
    </div>
</asp:Content>
