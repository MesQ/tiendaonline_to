﻿<%@ Page Title="Carrito" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cart.aspx.cs" Inherits="TiendaOnline.Web.Cart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
      <div id="cart" runat="server" class="ContentHead"><h1>Carrito de la Compra</h1></div>
    <asp:GridView ID="CartList" runat="server" AutoGenerateColumns="False" GridLines="Vertical" CellPadding="4"
        ItemType="TiendaOnline.CORE.Cart" SelectMethod="GetCartItems" 
        CssClass="table table-striped table-bordered" AllowSorting="True" OnRowCommand="grv_cart_RowCommand" >   
        <Columns>
                   
        <asp:BoundField DataField="Product_Id" HeaderText="Product_Id" SortExpression="Product_Id" Visible="False" />        
        <asp:BoundField DataField="Id" Visible="False" />
        <asp:BoundField DataField="Product.Name" HeaderText="Nombre" />        
        <asp:BoundField DataField="Product.Price" HeaderText="Precio (unidad)" DataFormatString="{0:c}"/>
            <asp:BoundField DataField="Product.Tax.Name" HeaderText="Impuesto" DataFormatString="{0:c}"/>  
        <asp:TemplateField   HeaderText="Cantidad">            
                <ItemTemplate>
                    <asp:TextBox ID="PurchaseQuantity" Width="40" runat="server" Text="<%#: Item.Quantity %>"></asp:TextBox> 
                </ItemTemplate>        
        </asp:TemplateField>    
        <asp:TemplateField HeaderText="Precio Total">            
                <ItemTemplate>
                    <%#: String.Format("{0:c}", ((Convert.ToDouble(Item.Quantity)) *  Convert.ToDouble(Item.Product.Price)))%>
                </ItemTemplate>        
        </asp:TemplateField> 
        <asp:TemplateField HeaderText="Opciones">            
                <ItemTemplate>
                    <a href="/ProductDetails?product=<%#: Item.Product_Id %>" class="btn btn-success">Ver</a>
                    &nbsp
                    <asp:Button ID="btnUpdate" runat="server" Text="Actualizar" CssClass="btn btn-primary" CommandArgument='<%# Container.DisplayIndex + ";" + Eval("Id") %>' />
                    &nbsp
                    <a href="/CartRemove?productId=<%#: Item.Id %>" class="btn btn-danger">Eliminar</a>
                </ItemTemplate>        
        </asp:TemplateField>    
        </Columns>    
    </asp:GridView>
    <div>
        <p></p>
        <strong>
            <asp:Label ID="lblTaxName" runat="server" Text="Total Impuestos: "></asp:Label>
            <asp:Label ID="lblTax" runat="server" EnableViewState="False" ForeColor="Blue"></asp:Label>
        </strong> <br />
                <strong>
            <asp:Label ID="Label1" runat="server" Text="Total Pedido: "></asp:Label>
            <asp:Label ID="lblTotalPrice" runat="server" EnableViewState="false" ForeColor="Blue"></asp:Label>
        </strong> 
    </div>
    <br />
    <table> 
    <tr>
      <td>
          <asp:Button ID="btnCheckout" runat="server" CssClass="btn btn-success" Text="Comprar" OnClick="btnCheckout_Click" />
      </td>
    </tr>
    </table>
</asp:Content>
