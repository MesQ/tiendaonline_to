﻿using TiendaOnline.Application;
using TiendaOnline.CORE;
using TiendaOnline.DAL;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TiendaOnline.Web.Admin.Tax
{
    public partial class Edit : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        TaxManager taxManager = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            taxManager = new TaxManager(context);
            //carga el id desde la url
            int id = 0;
            if (Request.QueryString["id"] != null)
            {
                if (int.TryParse(Request.QueryString["id"], out id))
                {
                    var tax = taxManager.GetById(id);
                    if (tax != null)
                    {
                        if (!Page.IsPostBack)
                            LoadTax(tax);
                    }
                }
            }
        }

        /// <summary>
        /// Metodo que carga los datos del impuesto en los campos
        /// </summary>
        /// <param name="tax">impuesto</param>
        private void LoadTax(TiendaOnline.CORE.Tax tax)
        {
            txtId.Value = tax.Id.ToString();
            txtName.Text = tax.Name;
            txtPercentage.Text = tax.Percentage.ToString();

        }

        /// <summary>
        /// Metodo que la hacer click sobre el boton actualiza los datos del impuesto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var tax = new TiendaOnline.CORE.Tax
                {
                    Id = int.Parse(txtId.Value),
                    Name = txtName.Text,
                    Percentage = decimal.Parse(txtPercentage.Text)
                };

                taxManager.Update(tax);
                taxManager.Context.SaveChanges();

                //saca el aviso de actualizacion correcta y redirige a la pagina inicial 
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Impuesto actualizado correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);


            }
            catch (Exception ex)
            {

                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al modificar, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);

                //escribe en el log
                string path = Server.MapPath("~/Logs/Errors.txt");
                taxManager.ErrorLog(path, ex.Message);
            }
        }
    }
}