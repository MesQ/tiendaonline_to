﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.DAL;

namespace TiendaOnline.Web.Admin.Tax
{
    public partial class Remove : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        TaxManager taxManager = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            taxManager = new TaxManager(context);
            //carga el id desde la url
            int id = 0;
            if (Request.QueryString["id"] != null)
            {
                if (int.TryParse(Request.QueryString["id"], out id))
                {
                    var tax = taxManager.GetById(id);
                    if (tax != null)
                    {
                        if (!Page.IsPostBack)
                            LoadTax(tax);
                    }
                }
            }
        }
        /// <summary>
        /// Metodo que obtiene los datos y los carga en los campos
        /// </summary>
        /// <param name="tax">impuesto</param>
        private void LoadTax(TiendaOnline.CORE.Tax tax)
        {
            lblTax.Text = tax.Name.ToString();
            txtId.Value = tax.Id.ToString();
        }

        /// <summary>
        /// Metodo que la hacer click sobre el boton confirma la elimnacion de la entiodad
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                var tax = taxManager.GetById(int.Parse(txtId.Value));
                taxManager.Remove(tax);
                context.SaveChanges();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Impuesto eliminado correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);

            }
            catch (Exception ex)
            {

                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al modificar, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                //escribe en el log
                string path = Server.MapPath("~/Logs/Errors.txt");
                taxManager.ErrorLog(path, ex.Message);
            }
        }
    }
}