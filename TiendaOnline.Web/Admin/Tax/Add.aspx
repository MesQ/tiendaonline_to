﻿<%@ Page Title="Añadir Impuesto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="TiendaOnline.Web.Admin.Tax.Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <h3>Añadir Impuesto</h3>
        <br />
        <br />
        <div class="form-horizontal">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />
        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="Nombre:"></asp:Label><br />
            <asp:TextBox ID="name" CssClass="form-control input-md" runat="server"></asp:TextBox><br />
            <asp:RequiredFieldValidator
                id="reqName"
                ErrorMessage="El nombre es obligatorio"
                ControlToValidate="name"
                Runat="server" CssClass="alert alert-error" />
        </div>

        <div class="form-group">
            <asp:Label ID="Label4" runat="server" Text="Porcentaje:"></asp:Label><br />
            <asp:TextBox ID="percentage" CssClass="form-control input-md" runat="server"></asp:TextBox><br />
                <asp:RequiredFieldValidator
                id="RequiredFieldValidator2"
                ErrorMessage="El porcentaje es obligatorio"
                ControlToValidate="percentage"
                Runat="server" CssClass="alert alert-error" />
        </div>

        <div class="form-group">
            <asp:Button ID="submit" CssClass="btn btn-primary" runat="server" Text="Enviar" OnClick="submit_Click" />
            <a href="Default"
                <button  id="VolverBtn" class="btn btn-danger">Volver</button>
            </a>
        </div>
        </div>
</asp:Content>
