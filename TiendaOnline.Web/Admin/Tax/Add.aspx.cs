﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.DAL;
using TiendaOnline.CORE;

namespace TiendaOnline.Web.Admin.Tax
{
    public partial class Add : System.Web.UI.Page
    {
        private TaxManager manager;

        protected void Page_Load(object sender, EventArgs e)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            manager = new TaxManager(context);
        }

        /// <summary>
        /// Metodo que al hacer click añade un nuevo impuesto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void submit_Click(object sender, EventArgs e)
        {
            try
            {
                TiendaOnline.CORE.Tax tax = new TiendaOnline.CORE.Tax
                {
                    Name = name.Text,
                    Percentage = decimal.Parse(percentage.Text)
                };

                manager.Add(tax);
                manager.Context.SaveChanges();
                //saca aviso de que el impuesto ha sido añadido y redirige a la pagina inicial de impuestos
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Impuesto añadido correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);
            }
            catch (Exception ex)
            {
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al modificar, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                
                //escribe ene el log
                string path = Server.MapPath("~/Logs/Errors.txt");
                manager.ErrorLog(path, ex.Message);
            }
        }
    }
}