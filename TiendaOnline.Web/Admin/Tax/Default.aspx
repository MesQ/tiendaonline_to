﻿<%@ Page Title="Admin Impuestos" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TiendaOnline.Web.Admin.Taxes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />

            <h3>Admin Impuestos</h3><br />
        <table id="Taxes" class="table table-striped table-bordered table-hover" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Porcentaje</th>
                <th>Opciones</th>                            
            </tr>
        </thead>
    </table>
    <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success" runat="server" PostBackUrl="~/Admin/Tax/Add.aspx">Añadir</asp:LinkButton>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        $("#Taxes").DataTable({
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '/Admin/Tax/ServiceList.ashx',
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
            },
            "columns": [                    
                    { "data": "Id", "Name": "Id", "autoWidth": true },
                    { "data": "Name", "Name": "Name", "autoWidth": true },
                    { "data": "Percentage", "Name": "Percentage", "autoWidth": true },
            ],
            "columnDefs": [
                    {
                            "render": function (data, type, row) {
                                return "<a href='/Admin/Tax/Edit?id=" + row.Id + "' class='btn btn-warning btn-sm'>Editar</a> <a href='/Admin/Tax/Remove?id=" + row.Id + "' class='btn btn-danger btn-sm'>Eliminar</a>";                       
                        },                        
                        "targets": 3
                    }
            ]
        });
    });
        </script>


</asp:Content>


