﻿<%@ Page Title="Admin Logs" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TiendaOnline.Web.Admin.Logs.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3>Visor Logs</h3><br />
    <asp:TextBox ID="txtLogs" CssClass="form-control input-lg" runat="server" TextMode="MultiLine" Height="500px"></asp:TextBox>
</asp:Content>
