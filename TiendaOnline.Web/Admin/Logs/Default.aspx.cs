﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TiendaOnline.Web.Admin.Logs
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //abrimos el archivo y lo leemos al textbox
            string path = Server.MapPath("~/Logs/Errors.txt");
            StreamReader sr = new StreamReader(path);
            txtLogs.Text = sr.ReadToEnd();
        }
    }
}