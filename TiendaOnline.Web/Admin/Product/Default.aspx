﻿<%@ Page Title="Admin Productos" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TiendaOnline.Web.Admin.Products" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />

            <h3>Admin Productos</h3><br />
        <table id="Products" class="table table-striped table-bordered table-hover" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Descripción</th>
                <th>Precio</th>
                <th>Categoria</th>
                <th>Impuesto</th>
                <th>Stock</th>
                <th>Opciones</th>                            
            </tr>
        </thead>
    </table>
    <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success" runat="server" PostBackUrl="~/Admin/Product/Add.aspx">Añadir</asp:LinkButton>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        $("#Products").DataTable({
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '/Admin/Product/ServiceList.ashx',
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
            },
            "columns": [                    
                    { "data": "Id", "Name": "Id", "autoWidth": true },
                    { "data": "Name", "Name": "Name", "autoWidth": true },
                    { "data": "Description", "Name": "Description", "autoWidth": true },
                    { "data": "Price", "Name": "Price", "autoWidth": true },
                    { "data": "Category", "Name": "Category", "autoWidth": true },
                    { "data": "Tax", "Name": "Tax", "autoWidth": true },
                    { "data": "Stock", "Name": "Stock", "autoWidth": true },
            ],
            "columnDefs": [
                    {
                            "render": function (data, type, row) {
                                return "<a href='/ProductDetails?product=" + row.Id + "' class='btn btn-primary btn-sm'>Ver Producto</a> <a href='/Admin/Product/Edit?id=" + row.Id + "' class='btn btn-warning btn-sm'>Editar</a> <a href='/Admin/Product/Remove?id=" + row.Id + "' class='btn btn-danger btn-sm'>Eliminar</a>";
                        },                        
                        "targets": 7
                    }
            ]
        });
    });
        </script>


</asp:Content>


