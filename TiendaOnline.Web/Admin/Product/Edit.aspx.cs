﻿using TiendaOnline.Application;
using TiendaOnline.CORE;
using TiendaOnline.DAL;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;

namespace TiendaOnline.Web.Admin.Product
{
    public partial class Edit : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        ProductManager manager = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            manager = new ProductManager(context);
            if (!IsPostBack){BindDropdown(); }

        //Carga el id desde la url
        int id = 0;
            if (Request.QueryString["id"] != null)
            {
                if (int.TryParse(Request.QueryString["id"], out id))
                {
                    var product = manager.GetById(id);
                    if (product != null)
                    {

                        if (!Page.IsPostBack)
                            
                            LoadProduct(product);
                    }
                }
            }
        }

        /// <summary>
        /// Metodo que carga los datos en los campos
        /// </summary>
        /// <param name="product">producto</param>
        private void LoadProduct(TiendaOnline.CORE.Product product)
        {
            txtId.Value = product.Id.ToString();
            txtName.Text = product.Name;
            txtDescription.Text = product.Description;
            txtPrice.Text = product.Price.ToString();
            category.SelectedValue = category.SelectedValue;
            tax.SelectedValue = tax.SelectedValue;
            txtStock.Text = product.Stock.ToString();
            txtPathFile.Value = product.PathFile.ToString();
        }

        /// <summary>
        /// Metodo que al hacer click actualiza los datos de la entodad seleccionada
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {

                var product = new TiendaOnline.CORE.Product
                {
                    Id = int.Parse(txtId.Value),
                    Name = txtName.Text,
                    Description = txtDescription.Text,
                    Price = decimal.Parse(txtPrice.Text),
                    Category_Id = int.Parse(category.SelectedValue),
                    Tax_Id = int.Parse(tax.SelectedValue),
                    Stock = int.Parse(txtStock.Text),
                    PathFile = txtPathFile.Value
                };

                manager.Update(product);
                manager.Context.SaveChanges();

                //saca aviso de que se ha editado correctamente y devuelve a la pagina principal
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Producto actualizado correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);


            }
            catch (Exception ex)
            {
                //Muestra error si no se ejecuta el try
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al modificar, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                //Escribe el log en el fichero
                string path = Server.MapPath("~/Logs/Errors.txt");
                manager.ErrorLog(path, ex.Message);
            }
        }

        /// <summary>
        /// Metodo que carga los dropdownlists
        /// </summary>
        public void BindDropdown()
        {
            //Dropdownlist categoria
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from Categories", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            category.DataSource = dt;
            category.DataTextField = "Name";
            category.DataValueField = "Id";
            category.DataBind();

            //Dropdownlist impuesto
            DataTable dt2 = new DataTable();
            using (SqlConnection con2 = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                con2.Open();
                SqlCommand cmd2 = new SqlCommand("Select * from Taxes", con2);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                da2.Fill(dt2);
            }

            tax.DataSource = dt2;
            tax.DataTextField = "Name";
            tax.DataValueField = "Id";
            tax.DataBind();

        }
    }
}