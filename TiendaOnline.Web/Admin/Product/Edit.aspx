﻿<%@ Page Title="Editar Producto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="TiendaOnline.Web.Admin.Product.Edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
 <h3>Editar Producto</h3>
        <asp:HiddenField ID="txtId" runat="server" />
        <asp:HiddenField ID="txtPathFile" runat="server" />
        <div class="form-horizontal">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />
        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="Nombre:"></asp:Label><br />
            <asp:TextBox ID="txtName" CssClass="form-control input-md" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator
                id="reqName"
                ErrorMessage="El nombre es obligatorio"
                ControlToValidate="txtName"
                Runat="server" CssClass="alert alert-error" />
        </div>

        <div class="form-group">
            <asp:Label ID="Label4" runat="server" Text="Descripción:"></asp:Label><br />
            <asp:TextBox ID="txtDescription" CssClass="form-control input-md" TextMode="multiline" runat="server"></asp:TextBox>
        </div>

        <div class="form-group">
            <asp:Label ID="Label5" runat="server" Text="Precio:"></asp:Label><br />
            <asp:TextBox ID="txtPrice" CssClass="form-control input-md" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator
                id="RequiredFieldValidator3"
                ErrorMessage="El precio es obligatorio"
                ControlToValidate="txtPrice"
                Runat="server" CssClass="alert alert-error" />
        </div>

        <div class="form-group">
            <asp:Label ID="Label2" runat="server" Text="Categoria:"></asp:Label><br />
            <asp:DropDownList ID="category" CssClass="form-control" runat="server" AutoPostBack="True"></asp:DropDownList>
            <asp:RequiredFieldValidator
                id="RequiredFieldValidator1"
                ErrorMessage="La categoria es obligatoria"
                ControlToValidate="category"
                Runat="server" CssClass="alert alert-error" />
        </div>

         <div class="form-group">
            <asp:Label ID="Label6" runat="server" Text="Impuesto:"></asp:Label><br />
            <asp:DropDownList ID="tax" CssClass="form-control" runat="server"></asp:DropDownList>
            <asp:RequiredFieldValidator
                id="RequiredFieldValidator4"
                ErrorMessage="El impuesto es obligatorio"
                ControlToValidate="tax"
                Runat="server" CssClass="alert alert-error" />
        </div>

        <div class="form-group">
            <asp:Label ID="Label3" runat="server" Text="Stock:"></asp:Label><br />
            <asp:TextBox ID="txtStock" CssClass="form-control input-md" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator
                id="RequiredFieldValidator2"
                ErrorMessage="El precio es obligatorio"
                ControlToValidate="txtStock"
                Runat="server" CssClass="alert alert-error" />
        </div>


        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary" runat="server" Text="Enviar" OnClick="btnSubmit_Click" />
            <a href="Default"                
                <button  id="VolverBtn" class="btn btn-danger">Volver</button>
            </a>
        </div>
        </div>
</asp:Content>
