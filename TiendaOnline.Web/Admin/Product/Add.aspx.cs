﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.DAL;
using TiendaOnline.CORE;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace TiendaOnline.Web.Admin.Product
{
    public partial class Add : System.Web.UI.Page
    {
        private ProductManager proManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            proManager = new ProductManager(context);

            if (!IsPostBack)
            {
                BindDropdown();
            }     

        }
        /// <summary>
        /// Metodo que carga los dropdownlists
        /// </summary>
        public void BindDropdown()
        {
            //carga dropdownlist de categorias
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from Categories", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            category.DataSource = dt;
            category.DataTextField = "Name";
            category.DataValueField = "Id";
            category.DataBind();

            //carga dropdownlist de impuesto
            DataTable dt2 = new DataTable();
            using (SqlConnection con2 = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                con2.Open();
                SqlCommand cmd2 = new SqlCommand("Select * from Taxes", con2);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                da2.Fill(dt2);
            }

            tax.DataSource = dt2;
            tax.DataTextField = "Name";
            tax.DataValueField = "Id";
            tax.DataBind();
        }

        /// <summary>
        /// Metodo que al hacer click añade un nuevo producto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //carga la imagen del producto
                string filename = Path.GetFileName(img.FileName);
                img.SaveAs(Server.MapPath("~/Catalog/Images/") + filename);

                TiendaOnline.CORE.Product pro = new TiendaOnline.CORE.Product
                {
                    Name = txtName.Text,
                    Description = txtDescription.Text,
                    Price = decimal.Parse(txtPrice.Text),
                    Category_Id = int.Parse(category.SelectedValue),
                    Tax_Id = int.Parse(tax.SelectedValue),
                    Stock = int.Parse(txtStock.Text),
                    PathFile = "Catalog/Images/" + filename

                };

                proManager.Add(pro);
                proManager.Context.SaveChanges();

                //Avisa al usuario que se ha añadido la categoria y redirige al la pagina inicial
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Producto añadido correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);
            }
            catch (Exception ex)
            {
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al añadir, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                //Ecribimos el error en un fichero
                string path = Server.MapPath("~/Logs/Errors.txt");
                proManager.ErrorLog(path, ex.Message);
            }
        }
    }
}