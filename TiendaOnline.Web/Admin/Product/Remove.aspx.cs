﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.DAL;

namespace TiendaOnline.Web.Admin.Product
{
    public partial class Remove : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        ProductManager manager = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            manager = new ProductManager(context);
            //Configura los parametros de la url Edit?id=
            int id = 0;
            if (Request.QueryString["id"] != null)
            {
                if (int.TryParse(Request.QueryString["id"], out id))
                {
                    var product = manager.GetById(id);
                    if (product != null)
                    {
                        if (!Page.IsPostBack)
                            LoadProduct(product);
                    }
                }
            }
        }

        /// <summary>
        /// Metodo que obtiene los datos y los carga en los campos
        /// </summary>
        /// <param name="product">producto</param>
        private void LoadProduct(TiendaOnline.CORE.Product product)
        {
            lblProduct.Text = product.Name.ToString();
            txtId.Value = product.Id.ToString();
        }

        /// <summary>
        /// Metodo que la hacer click sobre el boton confirm ala eliminacion de la entidad
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {

                var product = manager.GetById(int.Parse(txtId.Value));
                manager.Remove(product);
                context.SaveChanges();

                //muestra aviso de elminiacion correcta y redirige a pagina principal
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Producto eliminado correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);

            }
            catch (Exception ex)
            {
                
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al eliminar, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                string path = Server.MapPath("~/Logs/Errors.txt");
                manager.ErrorLog(path, ex.Message);
            }
        }
    }
}