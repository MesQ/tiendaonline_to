﻿using TiendaOnline.Application;
using TiendaOnline.DAL;
using TiendaOnline.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Script.Serialization;

namespace TiendaOnline.Web.Admin.User
{
    /// <summary>
    /// Obtiene los resultado de usuarios
    /// </summary>
    public class ServiceList : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            // Those parameters are sent by the plugin
            var iDisplayLength = int.Parse(context.Request["iDisplayLength"]);
            var iDisplayStart = int.Parse(context.Request["iDisplayStart"]);
            var sSearch = context.Request["sSearch"];
            var iSortDir = context.Request["sSortDir_0"];
            var iSortCol = context.Request["iSortCol_0"];
            var sortColum = context.Request.QueryString["mDataProp_" + iSortCol];


            ApplicationDbContext contextdb = new ApplicationDbContext();
            UserManager manager = new UserManager(contextdb);

            #region select
            var allUsers = manager.GetAll();
            var users = allUsers
                    .Select(p => new AdminUserList
                    {
                        Id = p.Id,
                        UserName = p.UserName,
                        Email = p.Email,
                        PhoneNumber = p.PhoneNumber
                    });
            #endregion
            #region Filter
            if (!string.IsNullOrWhiteSpace(sSearch))
            {
                string where = @"Id.ToString().Contains(@0) ||
                                 UserName.ToString().Contains(@0) ||
                                 Email.ToString().Contains(@0) ||
                                 PhoneNumber.ToString().Contains(@0)";
                users = users.Where(where, sSearch);
            }
            #endregion
            #region Paginate
            users = users
                        .OrderBy(sortColum + " " + iSortDir)
                        .Skip(iDisplayStart)
                        .Take(iDisplayLength);
            #endregion
            var result = new
            {
                iTotalRecords = allUsers.Count(),
                iTotalDisplayRecords = allUsers.Count(),
                aaData = users
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}