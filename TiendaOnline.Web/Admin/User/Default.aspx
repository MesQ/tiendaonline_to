﻿<%@ Page Title="Admin Usuarios" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TiendaOnline.Web.Admin.Users" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
            <h3>Admin Usuarios</h3><br />
        <table id="Users" class="table table-striped table-bordered table-hover" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Usuario</th>
                <th>Email</th>
                <th>Telefono</th>
                <th>Opciones</th>                            
            </tr>
        </thead>
    </table>
    <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/Account/Register.aspx" CssClass="btn btn-success">Añadir</asp:LinkButton>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        $("#Users").DataTable({
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '/Admin/User/ServiceList.ashx',
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
            },
            "columns": [
                    { "data": "Id", "Name": "Id", "autoWidth": true },
                    { "data": "UserName", "Name": "UserName", "autoWidth": true },
                    { "data": "Email", "Name": "Email", "autoWidth": true },
                    { "data": "PhoneNumber", "Name": "PhoneNumber", "autoWidth": true },
            ],
            "columnDefs": [
                    {
                            "render": function (data, type, row) {
                                return "<a href='/Admin/User/ViewProfile?id=" + row.Id + "' class='btn btn-primary btn-sm'>Ver Perfil</a> <a href='/Admin/User/Edit?id=" + row.Id + "' class='btn btn-warning btn-sm'>Editar</a> <a href='/Admin/User/Remove?id=" + row.Id + "' class='btn btn-danger btn-sm'>Eliminar</a>";
                        },                        
                        "targets": 4
                    }
            ]
        });
    });
        </script>


</asp:Content>


