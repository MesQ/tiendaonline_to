﻿<%@ Page Title="Ver Perfil" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewProfile.aspx.cs" Inherits="TiendaOnline.Web.Admin.User.ViewProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3>Ver Perfil</h3>
       <div class="form-horizontal">
    <div class="form-group">
        <div class="col-md-4">
            <asp:Label ID="Label1" runat="server" Text="Nombre:"></asp:Label>
            <asp:TextBox ID="txtName" CssClass="form-control input-md" runat="server" Enabled="False"></asp:TextBox>
        </div>
        <div class="col-md-4">
            <asp:Label ID="Label4" runat="server" Text="DNI:"></asp:Label><br />
            <asp:TextBox ID="txtDNI" CssClass="form-control input-md" runat="server" Enabled="False"></asp:TextBox>
        </div>
        <div class="col-md-4">
            <asp:Label ID="Label5" runat="server" Text="Teléfono:"></asp:Label><br />
            <asp:TextBox ID="txtPhone" CssClass="form-control input-md" runat="server" Enabled="False"></asp:TextBox>
        </div>
    </div>
    

    <div class="form-group">
        <div class="col-md-4">
            <asp:Label ID="Label3" runat="server" Text="País:"></asp:Label><br />
            <asp:TextBox ID="txtCountry" CssClass="form-control input-md" runat="server" Enabled="False"></asp:TextBox>
        </div>
        <div class="col-md-4">
            <asp:Label ID="Label2" runat="server" Text="Provincia:"></asp:Label><br />
            <asp:TextBox ID="txtProvince" CssClass="form-control input-md" runat="server" Enabled="False"></asp:TextBox>
        </div>
        <div class="col-md-4">
            <asp:Label ID="Label6" runat="server" Text="Ciudad:"></asp:Label><br />
            <asp:TextBox ID="txtCity" CssClass="form-control input-md" runat="server" Enabled="False"></asp:TextBox>
        </div>
    </div>


    <div class="form-group">
        <div class="col-md-4">
            <asp:Label ID="Label7" runat="server" Text="Calle:"></asp:Label><br />
            <asp:TextBox ID="txtStreet" CssClass="form-control input-md" runat="server" Enabled="False"></asp:TextBox>
        </div>
        <div class="col-md-4">
            <asp:Label ID="Label8" runat="server" Text="Número, portal, escalera, puerta...:"></asp:Label><br />
            <asp:TextBox ID="txtStreetDetails" CssClass="form-control input-md" runat="server" Enabled="False"></asp:TextBox>
        </div>
    </div>
    </div>
    <asp:LinkButton ID="LinkButton1" CssClass="btn btn-danger" runat="server" PostBackUrl="~/Admin/User/Default.aspx">Volver</asp:LinkButton>
</asp:Content>
