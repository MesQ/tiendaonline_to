﻿<%@ Page Title="Editar Usuario" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="TiendaOnline.Web.Admin.User.Edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:HiddenField ID="txtId" runat="server" />
    <asp:HiddenField ID="txtEmailConfirmed" runat="server" />
    <asp:HiddenField ID="txtPhoneNumberConfirmed" runat="server" />
    <asp:HiddenField ID="txtTwoFactorEnabled" runat="server" />
    <asp:HiddenField ID="txtLockoutEnabled" runat="server" />
    <asp:HiddenField ID="txtAccessFailedCount" runat="server" />
    <asp:HiddenField ID="txtPasswordHash" runat="server" />
    <asp:HiddenField ID="txtSecurityStamp" runat="server" />
       <div class="form-horizontal">
           <h3>Editar Usuario:</h3>
    <div class="form-group">
      
            <asp:Label ID="Label1" runat="server" Text="Nombre de Usuario:"></asp:Label>
            <asp:TextBox ID="txtUserName" CssClass="form-control input-md" runat="server"></asp:TextBox>
      
            <asp:Label ID="Label4" runat="server" Text="Email:"></asp:Label><br />
            <asp:TextBox ID="txtEmail" CssClass="form-control input-md" runat="server"></asp:TextBox>


            <asp:Label ID="Label5" runat="server" Text="Rol:"></asp:Label><br />
            <asp:DropDownList ID="ddlRole" CssClass="form-control" runat="server"></asp:DropDownList>

        <asp:Label ID="Label2" runat="server" Text="Nueva Password:"></asp:Label><br />
        <asp:TextBox ID="txtPassword" CssClass="form-control input-md" runat="server" TextMode="Password"></asp:TextBox>

    </div>
           <asp:Button ID="btnSubmit" CssClass="btn btn-success" runat="server" Text="Guardar" OnClick="btnSubmit_Click" />
           <asp:LinkButton ID="LinkButton1" CssClass="btn btn-danger" runat="server" PostBackUrl="~/Admin/User/Default.aspx">Volver</asp:LinkButton>
           </div>

</asp:Content>
