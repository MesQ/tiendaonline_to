﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.DAL;

namespace TiendaOnline.Web.Admin.User
{
    public partial class Remove : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        UserManager manager = null;
        ProfileManager profile = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            manager = new UserManager(context);
            profile = new ProfileManager(context);

        }

        /// <summary>
        /// Metodo que la hacer click sobre el boton confirm ala eliminacion de la entidad
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                //carga id desde la url y obtiene el usuario
                string id = Request.QueryString["id"];
                var usuario = manager.GetByUserId(id);
                int idProfile = 0;
                 idProfile = profile.GetByClientId(usuario.Id);
                if (idProfile != 0)
                {
                    var prof = profile.GetById(idProfile);
                    profile.Remove(prof);
                }
                
                manager.Remove(usuario);              
                context.SaveChanges();

                //muestra aviso de elminiacion correcta y redirige a pagina principal
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Usuario eliminado correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);

            }
            catch (Exception ex)
            {

                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al borrar, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                string path = Server.MapPath("~/Logs/Errors.txt");
                manager.ErrorLog(path, ex.Message);
            }
        }
    }
}