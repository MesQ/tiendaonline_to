﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.CORE;
using TiendaOnline.DAL;

namespace TiendaOnline.Web.Admin.User
{
    public partial class Edit : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        UserManager userManager = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            userManager = new UserManager(context);

            if (!IsPostBack)
            {
                BindDropdown();
            }
            //Carga el id desde la url
            if (Request.QueryString["id"] != null)
            {

                var user = userManager.GetByUserId(Request.QueryString["id"]);
                if (user != null)
                {
                    if (!Page.IsPostBack)
                        LoadUser(user);
                }
            }
        }

        /// <summary>
        /// Metodo que carga los datos en los campos
        /// </summary>
        /// <param name="order">pedido</param>
        private void LoadUser(TiendaOnline.CORE.ApplicationUser user)
        {

            txtId.Value = user.Id;
            txtEmail.Text = user.Email;
            txtUserName.Text = user.UserName;
            txtPasswordHash.Value = user.PasswordHash;
            txtSecurityStamp.Value = user.SecurityStamp;
            var userMgr = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            ddlRole.SelectedValue = userMgr.GetRoles(user.Id).FirstOrDefault();
            txtEmailConfirmed.Value = user.EmailConfirmed.ToString();
            txtPhoneNumberConfirmed.Value = user.PhoneNumberConfirmed.ToString();
            txtTwoFactorEnabled.Value = user.TwoFactorEnabled.ToString();
            txtLockoutEnabled.Value = user.LockoutEnabled.ToString();
            txtAccessFailedCount.Value = user.AccessFailedCount.ToString();

        }

        /// <summary>
        /// Metodo que carga los dropdownlists
        /// </summary>
        public void BindDropdown()
        {
            //Dropdownlist roles
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from AspNetRoles", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            ddlRole.DataSource = dt;
            ddlRole.DataTextField = "Name";
            ddlRole.DataValueField = "Name";
            ddlRole.DataBind();
        }

        /// <summary>
        /// Metodo que al hacer click actualiza los datos de la entodad seleccionada
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {

                //comprobamos si el campo password tiene una nueva passord y la asignamos al usuario
                if (txtPassword.Text != null)
                {
                    ApplicationUser cUser = userManager.GetByUserId(txtId.Value);
                    string newPassword = txtPassword.Text;
                    var hasher = new PasswordHasher();
                    string hashedNewPassword = hasher.HashPassword(newPassword);
                    txtPasswordHash.Value = hashedNewPassword;
                }

                var user = new TiendaOnline.CORE.ApplicationUser
                {
                    Id = txtId.Value,
                    Email = txtEmail.Text,
                    UserName = txtUserName.Text,
                    EmailConfirmed = bool.Parse(txtEmailConfirmed.Value),
                    PhoneNumberConfirmed = bool.Parse(txtPhoneNumberConfirmed.Value),
                    TwoFactorEnabled = bool.Parse(txtTwoFactorEnabled.Value),
                    LockoutEnabled = bool.Parse(txtLockoutEnabled.Value),
                    AccessFailedCount = int.Parse(txtAccessFailedCount.Value),
                    SecurityStamp = txtSecurityStamp.Value,
                    PasswordHash = txtPasswordHash.Value
                };


                userManager.Update(user);
                userManager.Context.SaveChanges();


                //añadimos rol al usuario
                var roleStore = new RoleStore<IdentityRole>(context);
                var userMgr = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var roleMgr = new RoleManager<IdentityRole>(roleStore);
                IdentityResult IdUserResult;

                //miramos si el rol seleccionado es admin o registered.
                //si es admin eliminamos rol registered y si es registered eliminamos el rol admin
                if (ddlRole.SelectedValue == "Admin")
                {
                    if (!userMgr.IsInRole(userMgr.FindByEmail(txtEmail.Text).Id, ddlRole.SelectedValue))
                    {
                        IdUserResult = userMgr.AddToRole(userMgr.FindByEmail(txtEmail.Text).Id, ddlRole.SelectedValue);
                    }

                    userMgr.RemoveFromRole(userMgr.FindByEmail(txtEmail.Text).Id, "Registered");
                }
                else
                {
                    if (!userMgr.IsInRole(userMgr.FindByEmail(txtEmail.Text).Id, ddlRole.SelectedValue))
                    {
                        IdUserResult = userMgr.AddToRole(userMgr.FindByEmail(txtEmail.Text).Id, ddlRole.SelectedValue);
                    }

                    userMgr.RemoveFromRole(userMgr.FindByEmail(txtEmail.Text).Id, "Admin");
                }
                
               
                //saca aviso de que se ha editado correctamente y devuelve a la pagina principal
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Usuario actualizado correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);
            }
            catch (Exception ex)
            {
                //Muestra error si no se ejecuta el try
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al modificar, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                //Escribe el log en el fichero
                string path = Server.MapPath("~/Logs/Errors.txt");
                userManager.ErrorLog(path, ex.Message);
            }
        }
    }
}