﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.DAL;

namespace TiendaOnline.Web.Admin.User
{
    public partial class ViewProfile : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        ProfileManager manager = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            manager = new ProfileManager(context);
            //carga el id desde la url
            string id = Request.QueryString["id"];
            var userId = manager.GetByClientId(id);
            var profile = manager.GetById(userId);
            if (profile != null)
            {
                if (!IsPostBack)
                {
                    LoadProfile(profile);
                }

            }
        }

        /// <summary>
        /// Metodo que carga los datos en los campos
        /// </summary>
        /// <param name="profile">perfil</param>
        private void LoadProfile(TiendaOnline.CORE.Profile profile)
        {
            txtName.Text = profile.Name;
            txtDNI.Text = profile.DNI;
            txtPhone.Text = profile.Phone;
            txtCountry.Text = profile.Country;
            txtProvince.Text = profile.Province;
            txtCity.Text = profile.City;
            txtStreet.Text = profile.Street;
            txtStreetDetails.Text = profile.StreetDetail;
        }
    }
}