﻿using TiendaOnline.Application;
using TiendaOnline.DAL;
using TiendaOnline.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Script.Serialization;

namespace TiendaOnline.Web.Admin.Order
{
    /// <summary>
    /// Obtiene los pedidos
    /// </summary>
    public class ServiceList : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            // Those parameters are sent by the plugin
            var iDisplayLength = int.Parse(context.Request["iDisplayLength"]);
            var iDisplayStart = int.Parse(context.Request["iDisplayStart"]);
            var sSearch = context.Request["sSearch"];
            var iSortDir = context.Request["sSortDir_0"];
            var iSortCol = context.Request["iSortCol_0"];
            var sortColum = context.Request.QueryString["mDataProp_" + iSortCol];


            ApplicationDbContext contextdb = new ApplicationDbContext();
            OrderManager manager = new OrderManager(contextdb);

            #region select
            var allOrders = manager.GetAll();
            var orders = allOrders
                    .Select(p => new AdminOrderList
                    {
                        Id = p.Id,
                        Client = p.Client.Email,
                        Status = p.Status.ToString(),
                        PurchaseDate = p.PurchaseDate,
                        SendDate = p.SendDate,
                        Paid = p.Paid,
                        PaymentMethod = p.PaymentMethod.ToString(),
                        Shipped = p.Shipped,
                        Tracking = p.Tracking,
                        TotalPrice = p.TotalPrice
                    });
            #endregion
            #region Filter
            if (!string.IsNullOrWhiteSpace(sSearch))
            {
                string where = @"Id.ToString().Contains(@0) ||
                                 Status.ToString().Contains(@0) ||
                                 PurchaseDate.ToString().Contains(@0) ||
                                 SendDate.ToString().Contains(@0) ||
                                 Paid.ToString().Contains(@0) ||
                                 PaymentMethod.ToString().Contains(@0) ||
                                 Shipped.ToString().Contains(@0) ||
                                 Tracking.ToString().Contains(@0) ||
                                 TotalPrice.ToString().Contains(@0)";
                orders = orders.Where(where, sSearch);
            }
            #endregion
            #region Paginate
            orders = orders
                        .OrderBy(sortColum + " " + iSortDir)
                        .Skip(iDisplayStart)
                        .Take(iDisplayLength);
            #endregion
            var result = new
            {
                iTotalRecords = allOrders.Count(),
                iTotalDisplayRecords = allOrders.Count(),
                aaData = orders
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}