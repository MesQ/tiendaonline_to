﻿<%@ Page Title="Admin pedidos" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TiendaOnline.Web.Admin.Orders" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />

            <h3>Admin Pedidos</h3><br />
        <table id="Documents" class="table table-striped table-bordered table-hover" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Cliente</th>
                <th>Estado</th>
                <th>Fecha Compra</th>
                <th>Fecha Envio</th>
                <th>Pagado</th>
                <th>Metodo de Pago</th>
                <th>Enviado</th>
                <th>Numero Envio</th>
                <th>Total</th>
                <th>Opciones</th>                            
            </tr>
        </thead>
    </table>

    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        $("#Documents").DataTable({
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '/Admin/Order/ServiceList.ashx',
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
            },
            "columns": [                    
                    { "data": "Id", "Name": "Id", "autoWidth": true },
                    { "data": "Client", "Name": "Client", "autoWidth": true },
                    { "data": "Status", "Name": "Status", "autoWidth": true },
                    { "data": "PurchaseDate", "Name": "PurchaseDate", "autoWidth": true },
                    { "data": "SendDate", "Name": "SendDate", "autoWidth": true },
                    { "data": "Paid", "Name": "Paid", "autoWidth": true },
                    { "data": "PaymentMethod", "Name": "PaymentMethod", "autoWidth": true },
                    { "data": "Shipped", "Name": "Shipped", "autoWidth": true },
                    { "data": "Tracking", "Name": "Tracking", "autoWidth": true },
                    { "data": "TotalPrice", "Name": "TotalPrice", "autoWidth": true }
            ],

            "columnDefs": [
 
                    {
                        "render": function (data, type, row) {
                            return "<a href='/Admin/Order/ViewOrder?id=" + row.Id + "' class='btn btn-primary btn-sm'>Ver</a> <a href='/Admin/Order/Edit?id=" + row.Id + "' class='btn btn-warning btn-sm'>Editar</a> <a href='/Admin/Order/Remove?id=" + row.Id + "' class='btn btn-danger btn-sm'>Eliminar</a>";
                        },
                        "targets": 10
                    },
                    {
                        "render": function (data, type, row) {
                            var dateString = data.substr(6);
                            var currentTime = new Date(parseInt(dateString));
                            var month = currentTime.getMonth() + 1;
                            var day = currentTime.getDate();
                            var year = currentTime.getFullYear();
                            var date = day + "/" + month + "/" + year;
                            return date;
                        },
                        "targets": 3
                    },
                    {
                        "render": function (data, type, row) {
                            if (data == null) {
                                //var date = 00 + "/" + 00 + "/" + 00;
                                var date = "No hay datos";
                                return date;
                            } else {
                                var dateString = data.substr(6);
                                var currentTime = new Date(parseInt(dateString));
                                var month = currentTime.getMonth() + 1;
                                var day = currentTime.getDate();
                                var year = currentTime.getFullYear();
                                var date = day + "/" + month + "/" + year;
                                return date;
                            }
                        },
                        "targets": 4
                    },
                    {
                        "render": function (data, type, row) {
                            if (data == true) {
                                var date = "Si";
                            } else {
                                var date = "No";
                            }
                            return date;
                        },
                        "targets": 5
                    },
                    {
                        "render": function (data, type, row) {
                            if (data == true) {
                                var date = "Si";
                            } else {
                                var date = "No";
                            }
                            return date;
                        },
                        "targets": 7
                    },
                    {
                        "render": function (data, type, row) {
                            if (data == null) {
                                var date = "No hay datos";
                                return date;
                            } else {
                                return data;
                            }
                        },
                        "targets": 8
                    },

            ]
        });
    });
        </script>


</asp:Content>


