﻿using TiendaOnline.Application;
using TiendaOnline.CORE;
using TiendaOnline.DAL;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;

namespace TiendaOnline.Web.Admin.Order
{
    public partial class Edit : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        OrderManager manager = null;
        ProfileManager profileManager = null;
        UserManager userManager = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            manager = new OrderManager(context);
            profileManager = new ProfileManager(context);
            userManager = new UserManager(context);
            if (!IsPostBack)
            {
                BindDropdown();
            }
            //Configura los parametros de la url Edit?id= 
            int id = 0;
            if (Request.QueryString["id"] != null)
            {
                if (int.TryParse(Request.QueryString["id"], out id))
                {
                    var order = manager.GetById(id);
                    if (order != null)
                    {
                        if (!Page.IsPostBack)
                            LoadOrder(order);
                    }
                }
            }
        }

        /// <summary>
        /// Metodo que carga los datos en los campos
        /// </summary>
        /// <param name="order">pedido</param>
        private void LoadOrder(TiendaOnline.CORE.Order order)
        {
            txtId.Value = order.Id.ToString();
            txtProfileId.Value = order.Profile_Id.ToString();
            ddlStatus.SelectedValue = order.Status.ToString();
            calPurchaseDate.SelectedDate = order.PurchaseDate.Date;
            if (order.SendDate.HasValue)
            {
                calSendDate.SelectedDate = order.SendDate.Value;
            }  
            ddlPaid.SelectedValue = order.Paid.ToString();
            ddlPaymentMethod.SelectedValue = order.PaymentMethod.ToString();
            ddlShipped.SelectedValue = order.Shipped.ToString();
            ddlClient.SelectedValue = order.Client_Id;
            txtTracking.Text = order.Tracking;
            txtTotalPrice.Text = order.TotalPrice.ToString();
        }

        /// <summary>
        /// Metodo que al hacer click actualiza los datos de la entodad seleccionada
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var query = profileManager.Context.Profiles;
                int profile_id = query.Where(c => c.Client_Id == ddlClient.SelectedValue).Select(p => p.Id).FirstOrDefault();

                var order = new TiendaOnline.CORE.Order
                {
                    Id = int.Parse(txtId.Value),
                    Status = (OrderStatus)Enum.Parse(typeof(OrderStatus), ddlStatus.SelectedValue),
                    PurchaseDate = calPurchaseDate.SelectedDate.Date,
                    SendDate = calSendDate.SelectedDate.Date,
                    Paid = bool.Parse(ddlPaid.SelectedValue),
                    PaymentMethod = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), ddlPaymentMethod.SelectedValue),
                    Shipped = bool.Parse(ddlShipped.SelectedValue),
                    Tracking = txtTracking.Text,
                    Client_Id = ddlClient.SelectedValue,
                    Profile_Id = profile_id,
                    TotalPrice = decimal.Parse(txtTotalPrice.Text)
                };


                manager.Update(order);
                manager.Context.SaveChanges();

                //Envio de emails
                //email para cliente
                MailMessage mail = new MailMessage();
                System.Net.Mail.SmtpClient SmtpServer = new System.Net.Mail.SmtpClient("smtp.live.com");
                mail.From = new System.Net.Mail.MailAddress("tiendaonline.to@hotmail.com");
                string email = userManager.GetByUserId(order.Client_Id).Email;
                mail.To.Add(email);
                mail.Subject = "Pedido Actualizado";
                mail.Body = "Se ha actualizado tu pedido en nuestra web puedes seguir los detalles desde mi cuenta.";

                //envio del email
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("tiendaonline.to@hotmail.com", "1A2B3C4d5e");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);

                //saca aviso de que se ha editado correctamente y devuelve a la pagina principal
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Pedido actualizado correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);


            }
            catch (Exception ex)
            {
                //Muestra error si no se ejecuta el try
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al modificar, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                //Escribe el log en el fichero
                string path = Server.MapPath("~/Logs/Errors.txt");
                manager.ErrorLog(path, ex.Message);
            }
        }

        /// <summary>
        /// Metodo que carga los dropdownlists
        /// </summary>
        public void BindDropdown()
        {
            //Dropdownlist cliente
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from AspNetUsers", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            ddlClient.DataSource = dt;
            ddlClient.DataTextField = "UserName";
            ddlClient.DataValueField = "Id";
            ddlClient.DataBind();
            //dropdownlist de estado
            ddlStatus.DataSource = Enum.GetValues(typeof(OrderStatus));
            ddlStatus.DataBind();
            //dropdownlist de metodo de pago
            ddlPaymentMethod.DataSource = Enum.GetValues(typeof(PaymentMethod));
            ddlPaymentMethod.DataBind();
        }
    }
}