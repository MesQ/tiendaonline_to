﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.DAL;

namespace TiendaOnline.Web.Admin.Order
{
    public partial class Remove : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        OrderManager manager = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            manager = new OrderManager(context);
            //Carga el id desde la url
            int id = 0;
            if (Request.QueryString["id"] != null)
            {
                if (int.TryParse(Request.QueryString["id"], out id))
                {
                    var order = manager.GetById(id);
                    if (order != null)
                    {
                        if (!IsPostBack)
                            LoadOrder(order);
                    }
                }
            }
        }

        /// <summary>
        /// Metodo que obtiene los datos y los carga en los campos
        /// </summary>
        /// <param name="document">documento</param>
        private void LoadOrder(TiendaOnline.CORE.Order order)
        {
            txtId.Value = order.Id.ToString();

        }

        /// <summary>
        /// Metodo que la hacer click sobre el boton confirm ala eliminacion de la entidad
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                context = new ApplicationDbContext();
                manager = new OrderManager(context);
                var order = manager.GetById(int.Parse(txtId.Value));              
                manager.Remove(order);
                context.SaveChanges();

                //muestra aviso de elminiacion correcta y redirige a pagina principal
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Pedido eliminado correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);

            }
            catch (Exception ex)
            {

                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al eliminar, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                string path = Server.MapPath("~/Logs/Errors.txt");
                manager.ErrorLog(path, ex.Message);
            }
        }
    }
}