﻿<%@ Page Title="Editar Pedido" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="TiendaOnline.Web.Admin.Order.Edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <asp:HiddenField ID="txtId" runat="server" />
        <asp:HiddenField ID="txtProfileId" runat="server" />
       <div class="form-horizontal">
           <h3>Editar Pedido:</h3>
    <div class="form-group">
      
            <asp:Label ID="Label1" runat="server" Text="Estado:"></asp:Label>
            <asp:DropDownList ID="ddlStatus" CssClass="form-control" runat="server"></asp:DropDownList>
      
            <asp:Label ID="Label4" runat="server" Text="Fecha Compra:"></asp:Label><br />
            <asp:Calendar ID="calPurchaseDate"  runat="server"></asp:Calendar>


            <asp:Label ID="Label5" runat="server" Text="Fecha Envio:"></asp:Label><br />
            <asp:Calendar ID="calSendDate" runat="server"></asp:Calendar>

    </div>
    

    <div class="form-group">

            <asp:Label ID="Label3" runat="server" Text="Pagado:"></asp:Label><br />
            <asp:DropDownList ID="ddlPaid" CssClass="form-control" runat="server">
                <asp:ListItem Text="Si" Value="True" />
                <asp:ListItem Text="No" Value="False" />
            </asp:DropDownList>

            <asp:Label ID="Label2" runat="server" Text="Metodo de Pago:"></asp:Label><br />
            <asp:DropDownList ID="ddlPaymentMethod" CssClass="form-control" runat="server"></asp:DropDownList>

            <asp:Label ID="Label6" runat="server" Text="Enviado:"></asp:Label><br />
            <asp:DropDownList ID="ddlShipped" CssClass="form-control" runat="server">
                <asp:ListItem Text="Si" Value="True" />
                <asp:ListItem Text="No" Value="False" />
            </asp:DropDownList>    
        </div>


    <div class="form-group">

            <asp:Label ID="Label8" runat="server" Text="Número Envio:"></asp:Label><br />
            <asp:TextBox ID="txtTracking" CssClass="form-control input-md" runat="server"></asp:TextBox>

            <asp:Label ID="Label9" runat="server" Text="Cliente:"></asp:Label><br />
            <asp:DropDownList ID="ddlClient" CssClass="form-control" runat="server"></asp:DropDownList>

            <asp:Label ID="Label7" runat="server" Text="Precio Total:"></asp:Label><br />
            <asp:TextBox ID="txtTotalPrice" CssClass="form-control input-md" runat="server"></asp:TextBox>
        </div>
    </div>
    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-success" OnClick="btnSubmit_Click" Text="Guardar" />
    <asp:LinkButton ID="LinkButton1" CssClass="btn btn-danger" runat="server" PostBackUrl="~/Admin/Order/Default.aspx">Volver</asp:LinkButton>
</asp:Content>
