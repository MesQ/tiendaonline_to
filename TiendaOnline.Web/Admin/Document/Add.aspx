﻿<%@ Page Title="Añadir Documento" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="TiendaOnline.Web.Admin.Document.Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <h3>Añadir Documento</h3>

        <div class="form-horizontal">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />
        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="Nombre:"></asp:Label><br />
            <asp:TextBox ID="txtName" CssClass="form-control input-md" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator
                id="reqName"
                ErrorMessage="El nombre es obligatorio"
                ControlToValidate="txtName"
                Runat="server" CssClass="alert alert-error" />
        </div>

        <div class="form-group">
            <asp:Label ID="Label4" runat="server" Text="Descripción:"></asp:Label><br />
            <asp:TextBox ID="txtDescription" CssClass="form-control input-md" TextMode="multiline" runat="server"></asp:TextBox>
        </div>

        <div class="form-group">
            <asp:Label ID="Label2" runat="server" Text="Producto:"></asp:Label><br />
            <asp:DropDownList ID="product" CssClass="form-control" runat="server"></asp:DropDownList>
            <asp:RequiredFieldValidator
                id="RequiredFieldValidator1"
                ErrorMessage="El producto es obligatorio"
                ControlToValidate="product"
                Runat="server" CssClass="alert alert-error" />
        </div>

        <div class="form-group">
            <asp:Label ID="Label3" runat="server" Text="Archivos:"></asp:Label><br />
            <asp:FileUpload ID="files" runat="server" AllowMultiple="True" />
                <asp:RequiredFieldValidator
                id="RequiredFieldValidator2"
                ErrorMessage="La subida de archivos es obligatoria"
                ControlToValidate="files"
                Runat="server" CssClass="alert alert-error" />
        </div>

        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary" runat="server" Text="Enviar" OnClick="btnSubmit_Click" />
            <a href="Default"
                <button  id="VolverBtn" class="btn btn-danger">Volver</button>
            </a>
        </div>
        </div>
</asp:Content>
