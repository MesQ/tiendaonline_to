﻿using TiendaOnline.Application;
using TiendaOnline.CORE;
using TiendaOnline.DAL;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace TiendaOnline.Web.Admin.Document
{
    public partial class Edit : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        DocumentManager manager = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            manager = new DocumentManager(context);
            if (!IsPostBack) { BindDropdown(); }
            //Carga el id desde la url
            int id = 0;
            if (Request.QueryString["id"] != null)
            {
                if (int.TryParse(Request.QueryString["id"], out id))
                {
                    var document = manager.GetById(id);
                    if (document != null)
                    {
                        if (!Page.IsPostBack)
                            LoadDocument(document);
                    }
                }
            }
        }

        /// <summary>
        /// Metodo que carga los datos en los campos
        /// </summary>
        /// <param name="document">documento</param>
        private void LoadDocument(TiendaOnline.CORE.Document document)
        {
            txtId.Value = document.Id.ToString();
            txtName.Text = document.Name;
            txtDescription.Text = document.Description;
            txtPath.Value = document.PathFile;
            product.SelectedValue = document.Product_Id.ToString();
        }

        /// <summary>
        /// Metodo que al hacer click actualiza los datos de la entodad seleccionada
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var document = new TiendaOnline.CORE.Document
                {
                    Id = int.Parse(txtId.Value),
                    Name = txtName.Text,
                    Description = txtDescription.Text,
                    Product_Id = int.Parse(product.SelectedValue),
                    PathFile = txtPath.Value
                };

                manager.Update(document);
                manager.Context.SaveChanges();

                //saca aviso de que se ha editado correctamente y devuelve a la pagina principal
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Documento actualizado correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);


        }
            catch (Exception ex)
            {
                //Muestra error si no se ejecuta el try
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al modificar, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                //Escribe el log en el fichero
                string path = Server.MapPath("~/Logs/Errors.txt");
                manager.ErrorLog(path, ex.Message);
            }
}

        /// <summary>
        /// Metodo que carga los dropdownlists
        /// </summary>
        public void BindDropdown()
        {
            //Dropdownlist categoria
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from Products", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            product.DataSource = dt;
            product.DataTextField = "Name";
            product.DataValueField = "Id";
            product.DataBind();
        }
    }
}