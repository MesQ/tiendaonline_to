﻿using TiendaOnline.Application;
using TiendaOnline.DAL;
using TiendaOnline.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Script.Serialization;

namespace TiendaOnline.Web.Admin.Document
{
    /// <summary>
    /// Obtiene los resultado de documento
    /// </summary>
    public class ServiceList : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            // Those parameters are sent by the plugin
            var iDisplayLength = int.Parse(context.Request["iDisplayLength"]);
            var iDisplayStart = int.Parse(context.Request["iDisplayStart"]);
            var sSearch = context.Request["sSearch"];
            var iSortDir = context.Request["sSortDir_0"];
            var iSortCol = context.Request["iSortCol_0"];
            var sortColum = context.Request.QueryString["mDataProp_" + iSortCol];


            ApplicationDbContext contextdb = new ApplicationDbContext();
            DocumentManager manager = new DocumentManager(contextdb);

            #region select
            var allDocuments = manager.GetAll();
            var documents = allDocuments
                    .Select(p => new AdminDocumentList
                    {
                        Id = p.Id,
                        Name = p.Name,
                        Description = p.Description,
                        Product = p.Product.Name,
                        PathFile = p.PathFile
                    });
            #endregion
            #region Filter
            if (!string.IsNullOrWhiteSpace(sSearch))
            {
                string where = @"Id.ToString().Contains(@0) ||
                                 Name.ToString().Contains(@0) ||
                                 Description.ToString().Contains(@0) ||
                                 Product.ToString().Contains(@0) ||
                                 PathFile.ToString().Contains(@0)";
                documents = documents.Where(where, sSearch);
            }
            #endregion
            #region Paginate
            documents = documents
                        .OrderBy(sortColum + " " + iSortDir)
                        .Skip(iDisplayStart)
                        .Take(iDisplayLength);
            #endregion
            var result = new
            {
                iTotalRecords = allDocuments.Count(),
                iTotalDisplayRecords = allDocuments.Count(),
                aaData = documents
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}