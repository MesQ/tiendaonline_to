﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.DAL;
using TiendaOnline.CORE;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace TiendaOnline.Web.Admin.Document
{
    public partial class Add : System.Web.UI.Page
    {
        private DocumentManager docManager;
        private ProductManager proManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            docManager = new DocumentManager(context);
            proManager = new ProductManager(context);
            if (!IsPostBack) { BindDropdown(); }
            
        }

        /// <summary>
        /// Metodo que carga el dropdownlist
        /// </summary>
        public void BindDropdown()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from Products", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            product.DataSource = dt;
            product.DataTextField = "Name";
            product.DataValueField = "Id";
            product.DataBind();

        }

        /// <summary>
        /// Metodo que al hacer click añade un nuevo documento
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                // iniciamos la subida de la imagen

                    string filename = Path.GetFileName(files.FileName);
                    files.SaveAs(Server.MapPath("~/Catalog/Docs/") + filename);
                //crea documento
                TiendaOnline.CORE.Document doc = new TiendaOnline.CORE.Document
                {
                    Name = txtName.Text,
                    Description = txtDescription.Text,
                    Product_Id = int.Parse(product.SelectedValue),
                    PathFile = "Catalog/Docs/"+filename
                };

                docManager.Add(doc);
                docManager.Context.SaveChanges();

                //Avisa al usuario que se ha añadido la categoria y redirige al la pagina inicial
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Documento añadido correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);
            }
            catch (Exception ex)
            {
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al añadir, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                //Ecribimos el error en un fichero
                string path = Server.MapPath("~/Logs/Errors.txt");
                docManager.ErrorLog(path, ex.Message);
            }
        }
    }
}