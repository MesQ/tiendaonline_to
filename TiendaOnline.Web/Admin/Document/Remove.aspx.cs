﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.DAL;

namespace TiendaOnline.Web.Admin.Document
{
    public partial class Remove : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        DocumentManager manager = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            manager = new DocumentManager(context);
            //Carga el id desde la url
            int id = 0;
            if (Request.QueryString["id"] != null)
            {
                if (int.TryParse(Request.QueryString["id"], out id))
                {
                    var document = manager.GetById(id);
                    if (document != null)
                    {
                        if (!Page.IsPostBack)
                            LoadDocument(document);
                    }
                }
            }
        }

        /// <summary>
        /// Metodo que obtiene los datos y los carga en los campos
        /// </summary>
        /// <param name="document">documento</param>
        private void LoadDocument(TiendaOnline.CORE.Document document)
        {
            lblProduct.Text = document.Name.ToString();
            txtId.Value = document.Id.ToString();
        }

        /// <summary>
        /// Metodo que la hacer click sobre el boton confirm ala eliminacion de la entidad
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                var product = manager.GetById(int.Parse(txtId.Value));
                try { System.IO.File.Delete(product.PathFile); }
                catch
                {
                    var err = new CustomValidator
                    {
                        ErrorMessage = "Se ha producido un error al eliminar el documento, la ruta no existe o tiene otro nombre",
                        IsValid = false
                    };
                    Page.Validators.Add(err);
                }
                
                manager.Remove(product);
                context.SaveChanges();

                //muestra aviso de elminiacion correcta y redirige a pagina principal
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Documento eliminado correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);

            }
            catch (Exception ex)
            {
                
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al eliminar, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                //escribe el log
                string path = Server.MapPath("~/Logs/Errors.txt");
                manager.ErrorLog(path, ex.Message);
            }
        }
    }
}