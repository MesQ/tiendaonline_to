﻿<%@ Page Title="Admin Documentos" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TiendaOnline.Web.Admin.Documents" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />

            <h3>Admin Documentos</h3><br />
        <table id="Documents" class="table table-striped table-bordered table-hover" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Descripción</th>
                <th>Producto</th>
                <th>Ruta Archivo</th>
                <th>Opciones</th>                            
            </tr>
        </thead>
    </table>
    <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success" runat="server" PostBackUrl="~/Admin/Document/Add.aspx">Añadir</asp:LinkButton>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        $("#Documents").DataTable({
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '/Admin/Document/ServiceList.ashx',
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
            },
            "columns": [                    
                    { "data": "Id", "Name": "Id", "autoWidth": true },
                    { "data": "Name", "Name": "Name", "autoWidth": true },
                    { "data": "Description", "Name": "Description", "autoWidth": true },
                    { "data": "Product", "Name": "Product", "autoWidth": true },
                    { "data": "PathFile", "Name": "PathFile", "autoWidth": true }
            ],

            "columnDefs": [
                    {

                        "render": function (data, type, row) {
                            return "<a href='/" + row.PathFile + "' class='btn btn-pink'>" + data + "</a>";
                        },
                        "targets": 4,
                    },
                    {
                        "render": function (data, type, row) {
                            return "<a href='/Admin/Document/Edit?id=" + row.Id + "' class='btn btn-warning btn-sm'>Editar</a> <a href='/Admin/Document/Remove?id=" + row.Id + "' class='btn btn-danger btn-sm'>Eliminar</a>";
                        },
                        "targets": 5
                    }
            ]
        });
    });
        </script>


</asp:Content>


