﻿<%@ Page Title="Panel Admin" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TiendaOnline.Web.Admin.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3>Panel de Administración</h3><br />
    <div class="container">
        <div class="row">
            <a runat="server" href="~/Admin/Product/">
                <div class="col-md-3">
                    <div class="card-counter primary">
                        <i class="fa fa-shopping-bag"></i>
                        <span class="count-name">Productos</span>
                    </div>
                </div>
            </a>

            <a runat="server" href="~/Admin/Category/">
                <div class="col-md-3">
                    <div class="card-counter danger">
                        <i class="fa fa-archive"></i>
                        <span class="count-name">Categorias</span>
                    </div>
                </div>
            </a>

            <a runat="server" href="~/Admin/Order/">
                <div class="col-md-3">
                    <div class="card-counter success">
                        <i class="fa fa-shopping-cart"></i>
                        <span class="count-name">Pedidos</span>
                    </div>
                </div>
            </a>

            <a runat="server" href="~/Admin/User/">
                <div class="col-md-3">
                    <div class="card-counter info">
                        <i class="fa fa-users"></i>
                        <span class="count-name">Usuarios</span>
                    </div>
                </div>
        </div>
        </a>

        <div class="row">
            <a runat="server" href="~/Admin/Document/">
                <div class="col-md-3">
                    <div class="card-counter doc">
                        <i class="fa fa-file"></i>
                        <span class="count-name">Documentos</span>
                    </div>
                </div>
            </a>

            <a runat="server" href="~/Admin/Tax/">
                <div class="col-md-3">
                    <div class="card-counter tax">
                        <i class="fa fa-euro"></i>
                        <span class="count-name">Impuestos</span>
                    </div>
                </div>
            </a>
                        <a runat="server" href="~/Admin/Image/">
                <div class="col-md-3">
                    <div class="card-counter img">
                        <i class="fa fa-image"></i>
                        <span class="count-name">Imagenes</span>
                    </div>
                </div>
            </a>

                 <a runat="server" href="~/Admin/Logs/">
                <div class="col-md-3">
                    <div class="card-counter log">
                        <i class="fa fa-folder"></i>
                        <span class="count-name">Logs</span>
                    </div>
                </div>
            </a>

        </div>

    </div>

</asp:Content>
