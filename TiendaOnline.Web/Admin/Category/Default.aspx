﻿<%@ Page Title="Admin Categorias" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TiendaOnline.Web.Admin.Categories" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
            <h3>Admin Categorias</h3><br />
        <table id="Categories" class="table table-striped table-bordered table-hover" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Descripción</th>
                <th>Opciones</th>                            
            </tr>
        </thead>
    </table>
    <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success" runat="server" PostBackUrl="~/Admin/Category/Add.aspx">Añadir</asp:LinkButton>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        $("#Categories").DataTable({
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '/Admin/Category/ServiceList.ashx',
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
            },
            "columns": [                    
                    { "data": "Id", "Name": "Id", "autoWidth": true },
                    { "data": "Name", "Name": "Name", "autoWidth": true },
                    { "data": "Description", "Name": "Description", "autoWidth": true },
            ],
            "columnDefs": [
                    {
                            "render": function (data, type, row) {
                                return "<a href='/Admin/Category/Edit?id=" + row.Id + "' class='btn btn-warning btn-sm'>Editar</a> <a href='/Admin/Category/Remove?id=" + row.Id + "' class='btn btn-danger btn-sm'>Eliminar</a>";
                        },                        
                        "targets": 3
                    }
            ]
        });
    });
        </script>


</asp:Content>


