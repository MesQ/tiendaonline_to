﻿using TiendaOnline.Application;
using TiendaOnline.CORE;
using TiendaOnline.DAL;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TiendaOnline.Web.Admin.Category
{
    public partial class Edit : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        CategoryManager manager = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            manager = new CategoryManager(context);

            //carga el id desde la url 
            int id = 0;
            if (Request.QueryString["id"] != null)
            {
                if (int.TryParse(Request.QueryString["id"], out id))
                {
                    var category = manager.GetById(id);
                    if (category != null)
                    {
                        if (!Page.IsPostBack)
                            LoadCategory(category);
                    }
                }
            }
        }

        /// <summary>
        /// Metodo que carga los datos en los campos
        /// </summary>
        /// <param name="category">categoria</param>
        private void LoadCategory(TiendaOnline.CORE.Category category)
        {
            txtId.Value = category.Id.ToString();
            txtName.Text = category.Name;
            txtDescription.Text = category.Description;

        }

        /// <summary>
        /// Metodo que al hacer click actualiza los datos de la entodad seleccionada
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var category = new TiendaOnline.CORE.Category
                {
                    Id = int.Parse(txtId.Value),
                    Name = txtName.Text,
                    Description = txtDescription.Text
                };

                manager.Update(category);
                manager.Context.SaveChanges();

                //saca aviso de que se ha editado correctamente y devuelve a la pagina principal
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Categoria actualizada correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);
            }
            catch (Exception ex)
            {
                //Muestra error si no se ejecuta el try
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al modificar, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                //Escribe el log en el fichero
                string path = Server.MapPath("~/Logs/Errors.txt");
                manager.ErrorLog(path, ex.Message);
            }
        }
    }
}