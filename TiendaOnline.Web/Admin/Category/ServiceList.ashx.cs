﻿using TiendaOnline.Application;
using TiendaOnline.DAL;
using TiendaOnline.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Script.Serialization;

namespace TiendaOnline.Web.Admin.Category
{
    /// <summary>
    /// Obtiene los resultado de categoria
    /// </summary>
    public class ServiceList : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            // Those parameters are sent by the plugin
            var iDisplayLength = int.Parse(context.Request["iDisplayLength"]);
            var iDisplayStart = int.Parse(context.Request["iDisplayStart"]);
            var sSearch = context.Request["sSearch"];
            var iSortDir = context.Request["sSortDir_0"];
            var iSortCol = context.Request["iSortCol_0"];
            var sortColum = context.Request.QueryString["mDataProp_" + iSortCol];


            ApplicationDbContext contextdb = new ApplicationDbContext();
            CategoryManager manager = new CategoryManager(contextdb);

            #region select
            var allCategories = manager.GetAll();
            var categories = allCategories
                    .Select(p => new AdminCategoryList
                    {
                        Id = p.Id,
                        Name = p.Name,
                        Description = p.Description

                    });
            #endregion
            #region Filter
            if (!string.IsNullOrWhiteSpace(sSearch))
            {
                string where = @"Id.ToString().Contains(@0) ||
                                 Name.ToString().Contains(@0) ||
                                 Description.ToString().Contains(@0)";
                categories = categories.Where(where, sSearch);
            }
            #endregion
            #region Paginate
            categories = categories
                        .OrderBy(sortColum + " " + iSortDir)
                        .Skip(iDisplayStart)
                        .Take(iDisplayLength);
            #endregion
            var result = new
            {
                iTotalRecords = allCategories.Count(),
                iTotalDisplayRecords = allCategories.Count(),
                aaData = categories
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}