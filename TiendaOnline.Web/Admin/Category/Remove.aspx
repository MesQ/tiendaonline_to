﻿<%@ Page Title="Eliminar Categoria" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Remove.aspx.cs" Inherits="TiendaOnline.Web.Admin.Category.Remove" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <h3>Eliminar Categoria</h3>
    <asp:HiddenField ID="txtId" runat="server" />
        <p>Esta seguro que desea eliminar la categoria <asp:Label ID="lblTax" runat="server" Text=""></asp:Label> ?</p>
        <asp:Button ID="btnRemove" runat="server" Text="Eliminar" CssClass="btn btn-danger" OnClick="btnRemove_Click" />
            <a href="Default"
                <button  id="VolverBtn" class="btn btn-primary">Volver</button>
            </a>
        
        


</asp:Content>
