﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.DAL;

namespace TiendaOnline.Web.Admin.Category
{
    public partial class Remove : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        CategoryManager manager = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            manager = new CategoryManager(context);

            //Carga el id desde la url
            int id = 0;
            if (Request.QueryString["id"] != null)
            {
                if (int.TryParse(Request.QueryString["id"], out id))
                {
                    var category = manager.GetById(id);
                    if (category != null)
                    {
                        if (!Page.IsPostBack)
                            LoadCategory(category);
                    }
                }
            }
        }

        /// <summary>
        /// Metodo que obtiene los datos y los carga en los campos
        /// </summary>
        /// <param name="category">categoria</param>
        private void LoadCategory(TiendaOnline.CORE.Category category)
        {
            lblTax.Text = category.Name.ToString();
            txtId.Value = category.Id.ToString();
        }

        /// <summary>
        /// Metodo que la hacer click sobre el boton confirm ala eliminacion de la entidad
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {

                var category = manager.GetById(int.Parse(txtId.Value));
                manager.Remove(category);
                context.SaveChanges();

                //muestra aviso de elminiacion correcta y redirige a pagina principal
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Categoria eliminada correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);

            }
            catch (Exception ex)
            {
                //saca el error por pantalla si se ejecuta el catch
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al modificar, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                //escribe el log
                string path = Server.MapPath("~/Logs/Errors.txt");
                manager.ErrorLog(path, ex.Message);
            }
        }
    }
}