﻿<%@ Page Title="Editar Categoria" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="TiendaOnline.Web.Admin.Category.Edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <div class="form-horizontal">
            <h3>Editar Categoria</h3>
            <asp:HiddenField ID="txtId" runat="server" />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />
        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="Nombre:"></asp:Label><br />
            <asp:TextBox ID="txtName" CssClass="form-control input-md" runat="server"></asp:TextBox><br />
            <asp:RequiredFieldValidator
                id="reqName"
                ErrorMessage="El nombre es obligatorio"
                ControlToValidate="txtName"
                Runat="server" CssClass="alert alert-error" />
        </div>

        <div class="form-group">
            <asp:Label ID="Label4" runat="server" Text="Descripción:"></asp:Label><br />
            <asp:TextBox ID="txtDescription" CssClass="form-control input-md" TextMode="multiline" runat="server"></asp:TextBox><br />
        </div>

        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary" runat="server" Text="Enviar" OnClick="btnSubmit_Click" />
            <a href="Default"
                <button  id="VolverBtn" class="btn btn-danger">Volver</button>
            </a>
        </div>
        </div>
</asp:Content>
