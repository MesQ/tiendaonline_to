﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.DAL;
using TiendaOnline.CORE;

namespace TiendaOnline.Web.Admin.Category
{
    public partial class Add : System.Web.UI.Page
    {
        private CategoryManager manager;

        protected void Page_Load(object sender, EventArgs e)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            manager = new CategoryManager(context);
        }

        /// <summary>
        /// Metodo que al hacer click añade una nueva categoria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void submit_Click(object sender, EventArgs e)
        {
            try
            {
                TiendaOnline.CORE.Category category = new TiendaOnline.CORE.Category
                {
                    Name = txtName.Text,
                    Description = txtDescription.Text
                };

                manager.Add(category);
                manager.Context.SaveChanges();
                //Avisa al usuario que se ha añadido la categoria y redirige al la pagina inicial
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Categoria añadida correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);
            }
            catch (Exception ex)
            {
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al añadir, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                //Ecribimos el error en un fichero
                string path = Server.MapPath("~/Logs/Errors.txt");
                manager.ErrorLog(path, ex.Message);
            }
        }
    }
}