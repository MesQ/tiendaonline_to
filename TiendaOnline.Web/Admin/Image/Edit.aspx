﻿<%@ Page Title="Editar Producto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="TiendaOnline.Web.Admin.Image.Edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
 <h3>Editar Imagen</h3>
        <asp:HiddenField ID="txtId" runat="server" />
        <asp:HiddenField ID="txtPathFile" runat="server" />
        <div class="form-horizontal">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />
   
        <div class="form-group">
            <asp:Label ID="Label2" runat="server" Text="Producto:"></asp:Label><br />
            <asp:DropDownList ID="ddlProduct" CssClass="form-control" runat="server"></asp:DropDownList>
            <asp:RequiredFieldValidator
                id="RequiredFieldValidator1"
                ErrorMessage="Es obligatorio elegir el producto"
                ControlToValidate="ddlProduct"
                Runat="server" CssClass="alert alert-error" />
        </div>

        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary" runat="server" Text="Enviar" OnClick="btnSubmit_Click" />
            <a href="Default"                
                <button  id="VolverBtn" class="btn btn-danger">Volver</button>
            </a>
        </div>
        </div>
</asp:Content>
