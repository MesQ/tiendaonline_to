﻿<%@ Page Title="Eliminar Imagen" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Remove.aspx.cs" Inherits="TiendaOnline.Web.Admin.Image.Remove" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <h3>Eliminar Imagen</h3>
    <asp:HiddenField ID="txtId" runat="server" />
        <p>Esta seguro que desea eliminar la imagen <asp:Label ID="lblProduct" runat="server" Text="" Font-Bold="True"></asp:Label> ?</p>
        <asp:Button ID="btnRemove" runat="server" Text="Eliminar" CssClass="btn btn-danger" OnClick="btnRemove_Click" />
    <asp:LinkButton ID="LinkButton1" CssClass="btn btn-primary" runat="server" PostBackUrl="~/Admin/Image/Default.aspx">Volver</asp:LinkButton>
        
        


</asp:Content>
