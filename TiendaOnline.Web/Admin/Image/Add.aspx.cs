﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.DAL;
using TiendaOnline.CORE;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace TiendaOnline.Web.Admin.Image
{
    public partial class Add : System.Web.UI.Page
    {
        private ImagesManager imgManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            imgManager = new ImagesManager(context);
            if (!IsPostBack)
            {
                BindDropdown();
            }
            

        }
        /// <summary>
        /// Metodo que carga los dropdownlists
        /// </summary>
        public void BindDropdown()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from Products", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            ddlProduct.DataSource = dt;
            ddlProduct.DataTextField = "Name";
            ddlProduct.DataValueField = "Id";
            ddlProduct.DataBind();
        }

        /// <summary>
        /// Metodo que al hacer click añade un nuevo producto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //carga la imagen y guarda la ruta del fichero
                string filename = Path.GetFileName(img.FileName);
                img.SaveAs(Server.MapPath("~/Catalog/Images/") + filename);

                TiendaOnline.CORE.ProductImages proImg = new TiendaOnline.CORE.ProductImages
                {
                    PathFile = "Catalog/Images/" + filename,
                    Product_Id = int.Parse(ddlProduct.SelectedValue),

                };
                //añade la imagen
                imgManager.Add(proImg);
                imgManager.Context.SaveChanges();

                //Avisa al usuario que se ha añadido la categoria y redirige al la pagina inicial
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Imagen añadida correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);
            }
            catch (Exception ex)
            {
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al añadir, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                //Ecribimos el error en un fichero
                string path = Server.MapPath("~/Logs/Errors.txt");
                imgManager.ErrorLog(path, ex.Message);
            }
        }
    }
}