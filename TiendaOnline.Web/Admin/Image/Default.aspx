﻿<%@ Page Title="Admin Productos" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TiendaOnline.Web.Admin.Images" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />

    <h3>Admin Productos</h3>
    <br />
    <table id="Images" class="table table-striped table-bordered table-hover" style="width: 100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Ruta Imagen</th>
                <th>Imagen</th>
                <th>Opciones</th>
            </tr>
        </thead>
    </table>
    <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success" runat="server" PostBackUrl="~/Admin/Image/Add.aspx">Añadir</asp:LinkButton>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#Images").DataTable({
                'bProcessing': true,
                'bServerSide': true,
                'sAjaxSource': '/Admin/Image/ServiceList.ashx',
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
                },
                "columns": [
                        { "data": "Id", "Name": "Id", "autoWidth": true },
                        { "data": "PathFile", "Name": "PathFile", "autoWidth": true },
                ],
                "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return "<a href='/Admin/Image/Edit?id=" + row.Id + "' class='btn btn-warning btn-sm'>Editar</a> <a href='/Admin/Image/Remove?id=" + row.Id + "' class='btn btn-danger btn-sm'>Eliminar</a>";
                            },
                            "targets": 3
                        },
                            {
                               "render": function (data, type, row) {
                                   return "<img src='/" + row.PathFile + "' height='50' width='50'>";
                                  },
                                    "targets": 2
                            },
                            {
                                "render": function (data, type, row) {
                                    return "<a href='/" + row.PathFile + "' class='btn btn-pink'>" + data + "</a>";
                                },
                                "targets": 1
                            }
                ]
            });
        });
    </script>


</asp:Content>


