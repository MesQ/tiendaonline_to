﻿using TiendaOnline.Application;
using TiendaOnline.CORE;
using TiendaOnline.DAL;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;

namespace TiendaOnline.Web.Admin.Image
{
    public partial class Edit : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        ImagesManager manager = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            manager = new ImagesManager(context);

            if (!IsPostBack)
            {
                BindDropdown();
            }
            //Carga el id desde la url
            int id = 0;
            if (Request.QueryString["id"] != null)
            {
                if (int.TryParse(Request.QueryString["id"], out id))
                {
                    var img = manager.GetById(id);
                    if (img != null)
                    {
                        if (!Page.IsPostBack)
                            
                            LoadImage(img);
                    }
                }
            }
        }

        /// <summary>
        /// Metodo que carga los datos en los campos
        /// </summary>
        /// <param name="image">producto</param>
        private void LoadImage(TiendaOnline.CORE.ProductImages img)
        {
            txtId.Value = img.Id.ToString();
            txtPathFile.Value = img.PathFile;
            ddlProduct.SelectedValue = img.Product_Id.ToString();
        }

        /// <summary>
        /// Metodo que al hacer click actualiza los datos de la entodad seleccionada
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {

                var img = new TiendaOnline.CORE.ProductImages
                {
                    Id = int.Parse(txtId.Value),
                    Product_Id = int.Parse(ddlProduct.SelectedValue),
                    PathFile = txtPathFile.Value
                    
                };

                manager.Update(img);
                manager.Context.SaveChanges();

                //saca aviso de que se ha editado correctamente y devuelve a la pagina principal
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Imagen actualizada correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);


            }
            catch (Exception ex)
            {
                //Muestra error si no se ejecuta el try
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al modificar, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                //Escribe el log en el fichero
                string path = Server.MapPath("~/Logs/Errors.txt");
                manager.ErrorLog(path, ex.Message);
            }
        }

        /// <summary>
        /// Metodo que carga los dropdownlists
        /// </summary>
        public void BindDropdown()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from Products", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            ddlProduct.DataSource = dt;
            ddlProduct.DataTextField = "Name";
            ddlProduct.DataValueField = "Id";
            ddlProduct.DataBind();
        }

    }
}