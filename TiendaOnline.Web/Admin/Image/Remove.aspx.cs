﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.DAL;

namespace TiendaOnline.Web.Admin.Image
{
    public partial class Remove : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        ImagesManager manager = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            manager = new ImagesManager(context);
            //Carga la id desde la url
            int id = 0;
            if (Request.QueryString["id"] != null)
            {
                if (int.TryParse(Request.QueryString["id"], out id))
                {
                    var image = manager.GetById(id);
                    if (image != null)
                    {
                        if (!Page.IsPostBack)
                        {
                            LoadImage(image);
                        }

                    }
                }
            }
        }

        /// <summary>
        /// Metodo que obtiene los datos y los carga en los campos
        /// </summary>
        /// <param name="image">imagen</param>
        private void LoadImage(TiendaOnline.CORE.ProductImages image)
        {
            txtId.Value = image.Id.ToString();
        }

        /// <summary>
        /// Metodo que la hacer click sobre el boton confirm ala eliminacion de la entidad
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {

                var image = manager.GetById(int.Parse(txtId.Value));
                if (System.IO.File.Exists(Server.MapPath("~/") + image.PathFile))
                {
                    System.IO.File.Delete(Server.MapPath("~/") + image.PathFile);
                    manager.Remove(image);
                    context.SaveChanges();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('No se ha podido eliminar la imagen!');", true);
                }
                

                //muestra aviso de elminiacion correcta y redirige a pagina principal
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Imagen eliminada correctamente!'); setTimeout(function(){window.location.href ='Default'});", true);

            }
            catch (Exception ex)
            {
                
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al eliminar, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                string path = Server.MapPath("~/Logs/Errors.txt");
                manager.ErrorLog(path, ex.Message);
            }
        }
    }
}