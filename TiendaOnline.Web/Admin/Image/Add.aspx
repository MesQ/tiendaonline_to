﻿<%@ Page Title="Añadir Imagenes" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="TiendaOnline.Web.Admin.Image.Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <h3>Añadir Imagenes</h3>
        <br />
        <br />
        <div class="form-horizontal">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />
   
        <div class="form-group">
            <asp:Label ID="Label2" runat="server" Text="Producto:"></asp:Label><br />
            <asp:DropDownList ID="ddlProduct" CssClass="form-control" runat="server"></asp:DropDownList>
            <asp:RequiredFieldValidator
                id="RequiredFieldValidator1"
                ErrorMessage="Es obligatorio elegir el producto"
                ControlToValidate="ddlProduct"
                Runat="server" CssClass="alert alert-error" />
        </div>

        <div class="form-group">
            <asp:Label ID="Label7" runat="server" Text="Imagenes:"></asp:Label><br />
            <asp:FileUpload ID="img" runat="server" />
                        <asp:RequiredFieldValidator
                id="RequiredFieldValidator5"
                ErrorMessage="Tienes que insertar almenos 1 imagen"
                ControlToValidate="img"
                Runat="server" CssClass="alert alert-error" />
        </div>

        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary" runat="server" Text="Enviar" OnClick="btnSubmit_Click" />
            <a href="Default"                
                <button  id="VolverBtn" class="btn btn-danger">Volver</button>
            </a>
        </div>
        </div>
</asp:Content>
