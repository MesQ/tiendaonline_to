﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.DAL;

namespace TiendaOnline.Web
{
    public partial class CartRemove : System.Web.UI.Page
    {
        private CartManager cartManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            cartManager = new CartManager(context);

            //obtenemos el id desde la url
            string rawId = Request.QueryString["productId"];
            int productId;
            //con el id llamamos al metodo remove sino sacamos el error 
            if (!String.IsNullOrEmpty(rawId) && int.TryParse(rawId, out productId))
            {
                var producto = cartManager.GetById(productId);
                cartManager.Remove(producto);
                cartManager.Context.SaveChanges();
            }
            else
            {
                throw new Exception("ERROR : Es ilegal llamar a CartRemove sin id de producto");
            }

            

            Response.Redirect("/Cart");
        }
    }
}