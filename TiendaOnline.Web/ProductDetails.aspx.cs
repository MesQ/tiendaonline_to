﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.CORE;
using TiendaOnline.DAL;

namespace TiendaOnline.Web
{
    public partial class ProductDetails : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Metodo que obtiene los dato del producto
        /// </summary>
        /// <param name="Id">id del product</param>
        /// <returns>producto</returns>
        public IQueryable<Product> GetProduct([QueryString("product")] int? Id)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager manager = new ProductManager(context);
            //seleccionamos el producto a mostrar los detalles
            IQueryable<Product> query = manager.Context.Products;
            if (Id.HasValue && Id > 0)
            {
                query = query.Where(p => p.Id == Id);
            }
            else
            {
                query = null;
            }

            //comprbamos stock del producto
            int stock = 0;
            stock = query.Where(p => p.Id == Id).Select(p => p.Stock).FirstOrDefault();

            if (stock <= 0)
            {
                lblStock.Text = "No hay Stock";
            }

            //cargamos la galeria de imagenes
            BindImages(Id);

            return query;
        }

        /// <summary>
        /// Metodo que obtiene las imagenes del producto
        /// </summary>
        /// <param name="Id">id del producto</param>
        protected void BindImages(int? Id)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from ProductImages where Product_id=" + Id, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                
            }

            dlImages.DataSource = dt;
            dlImages.DataBind();

        }

        /// <summary>
        /// Metodo que obtiene los documentos asociados al producto
        /// </summary>
        /// <param name="Id">id de producto</param>
        /// <returns>documentos</returns>
        public IQueryable<Document> GetDocuments([QueryString("product")] int? Id)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            DocumentManager manager = new DocumentManager(context);

            IQueryable<Document> query = manager.Context.Documents;
            if (Id.HasValue && Id > 0)
            {
                query = query.Where(p => p.Product_Id == Id);
            }

            return query;
        }

    }
}
    
