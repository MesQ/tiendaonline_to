﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TiendaOnline.Web.Models
{
    public class CartItemUpdates
    {
        /// <summary>
        /// id del producto
        /// </summary>
        public int Product_Id;

        /// <summary>
        /// cantidad del producto
        /// </summary>
        public int Quantity;
    }
}