﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TiendaOnline.CORE;

namespace TiendaOnline.Web.Models
{
    public class AdminOrderList
    {
        /// <summary>
        /// id del pedido
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// usuario del pedido
        /// </summary>
        public string Client { get; set; }

        /// <summary>
        /// estado del pedido
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// fecha de compra del pedido
        /// </summary>
        public DateTime PurchaseDate { get; set; }

        /// <summary>
        /// fecha del envio del pedido
        /// </summary>
        public DateTime? SendDate { get; set; }

        /// <summary>
        /// estado del pago del pedido si o no
        /// </summary>
        public bool Paid { get; set; }

        /// <summary>
        /// metodo de pago del pedido
        /// </summary>
        public string PaymentMethod { get; set; }

        /// <summary>
        /// estado del envio del pedido si o no
        /// </summary>
        public bool Shipped { get; set; }

        /// <summary>
        /// Numero de envio del pedido
        /// </summary>
        public string Tracking { get; set; }

        /// <summary>
        /// precio total del pedido
        /// </summary>
        public decimal TotalPrice { get; set; }
    }
}