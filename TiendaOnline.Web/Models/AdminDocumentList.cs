﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TiendaOnline.Web.Models
{
    public class AdminDocumentList
    {
        /// <summary>
        /// id de documento
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre de documento
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// descripcion de documento
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// producto del documento
        /// </summary>
        public string Product { get; set; }

        /// <summary>
        /// Ruta del archivo del documento
        /// </summary>
        public string PathFile { get; set; }
    }
}