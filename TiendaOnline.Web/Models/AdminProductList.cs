﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TiendaOnline.Web.Models
{
    public class AdminProductList
    {
        /// <summary>
        /// id del producto
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// nombre del producto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// descripcion del producto
        /// </summary>
        public string Description { get; set; }
        
        /// <summary>
        /// precio del producto
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// categoria del producto
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// impuesto de producto
        /// </summary>
        public string Tax { get; set; }

        /// <summary>
        /// cantidad de stock del producto
        /// </summary>
        public decimal Stock { get; set; }
    }
}