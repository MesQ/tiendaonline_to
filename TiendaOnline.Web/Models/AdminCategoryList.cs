﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TiendaOnline.Web.Models
{
    public class AdminCategoryList
    {
        /// <summary>
        /// Id de categoria
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre de categoria
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// descripcion de categoria
        /// </summary>
        public string Description { get; set; }
    }
}