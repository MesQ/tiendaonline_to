﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TiendaOnline.Web.Models
{
    public class AdminImageList
    {
        /// <summary>
        /// Id de l aimagen
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Ruta del archivo de la imagen 
        /// </summary>
        public string PathFile { get; set; }

        /// <summary>
        /// Producto de la imagen
        /// </summary>
        public string Product { get; set; }
    }
}