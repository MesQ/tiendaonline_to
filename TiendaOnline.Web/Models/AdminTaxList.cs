﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TiendaOnline.Web.Models
{
    public class AdminTaxList
    {
        /// <summary>
        /// id del impuesto
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// nombre del impuesto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// porcentaje del impuesto
        /// </summary>
        public decimal Percentage { get; set; }
    }
}