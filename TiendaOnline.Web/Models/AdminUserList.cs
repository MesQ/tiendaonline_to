﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TiendaOnline.Web.Models
{
    public class AdminUserList
    {
        /// <summary>
        /// id del usuario
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// nombre de usuario
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// email del usuario
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// numero de telefono del usuario
        /// </summary>
        public string PhoneNumber { get; set; }
    }
}