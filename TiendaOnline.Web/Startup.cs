﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TiendaOnline.Web.Startup))]
namespace TiendaOnline.Web
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
