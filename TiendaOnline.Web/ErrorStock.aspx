﻿<%@ Page Title="Error Stock" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ErrorStock.aspx.cs" Inherits="TiendaOnline.Web.ErrorStock" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div align="center">
    <h1>                   Oops!</h1>
                <h2>                    No tenemos stock de este producto</h2>
                <div class="error-details">
                    Sentimos las molestias, puedes ponerte en contacto con nosotros y te avisaremos cuando lo tengamos disponible
                </div><br />
    <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/Products.aspx" CssClass="btn btn-primary">Volver a Productos</asp:LinkButton>&nbsp &nbsp
    <asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="~/Contact.aspx" CssClass="btn btn-success">Contacto</asp:LinkButton>
        </div>
</asp:Content>
