﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using TiendaOnline.Application;
using TiendaOnline.DAL;

namespace TiendaOnline.Web
{
    public partial class AddToCart : System.Web.UI.Page
    {
        private CartManager cartManager;
        private ProductManager productManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            cartManager = new CartManager(context);
            productManager = new ProductManager(context);

                //obtenemos el id desde la url
                string rawId = Request.QueryString["productId"];
                int productId;
                //con el id llamamos al metodo AddCart sino sacamos el error 
                if (!String.IsNullOrEmpty(rawId) && int.TryParse(rawId, out productId))
                {
                    AddCart(productId);                                             
                }
                else
                {
                    throw new Exception("ERROR : Es ilegal llamar a AddToCart sin id de producto");
                }
            
            
            Response.Redirect("/Cart");
        }
        /// <summary>
        /// Metodo que añade el producto al carrito
        /// </summary>
        /// <param name="id">id de producto</param>
        public void AddCart(int id)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            cartManager = new CartManager(context);
            productManager = new ProductManager(context);

            var product = productManager.GetById(id);
            //comprbamos el stock del producto si es 0 o menos devuelve error
            if (product.Stock <= 0)
            {
                Response.Redirect("/ErrorStock");
            }
            else
            {
                try
                {
                    //creamos el item para añadir
                    TiendaOnline.CORE.Cart cartItem = new TiendaOnline.CORE.Cart
                    {
                        SessionId = cartManager.GetCartId(),
                        Client_Id = User.Identity.GetUserId(),
                        Product_Id = product.Id,
                        Quantity = 1,
                        Date = DateTime.Now,
                        Price = product.Price

                    };
                    //añadimos el item y guardamos
                    cartManager.Add(cartItem);
                    cartManager.Context.SaveChanges();
                }
                catch (Exception ex)
                {
                    var err = new CustomValidator
                    {
                        ErrorMessage = "Se ha producido un error al añadir, si el error persiste contacte con el administrador",
                        IsValid = false
                    };
                    Page.Validators.Add(err);
                    //Ecribimos el error en un fichero
                    string path = Server.MapPath("~/Logs/Errors.txt");
                    cartManager.ErrorLog(path, ex.Message);
                }
            }
        }
    }
}