﻿<%@ Page Title="Productos" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Products.aspx.cs" Inherits="TiendaOnline.Web.ProductList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section>
        <div>


               <br /><div id="CategoryMenu" style="text-align: center">
                  <b style="font-size: large; font-style: normal"> <a href="Products" class="btn btn-primary">Todos</a></b> |
                <asp:ListView ID="categoryList"
                    ItemType="TiendaOnline.CORE.Category"
                    runat="server"
                    SelectMethod="GetCategories">
                    
                    <ItemTemplate>
                        <b style="font-size: large; font-style: normal">
                            <a href="/Products?id=<%#: Item.Id %>" class="btn btn-primary">
                                <%#: Item.Name %>
                            </a>
                        </b>
                    </ItemTemplate>
                       
                    <ItemSeparatorTemplate> | </ItemSeparatorTemplate>
                </asp:ListView>

            </div><br />

            <asp:ListView ID="productsList" runat="server"
                DataKeyNames="Id" GroupItemCount="4"
                ItemType="TiendaOnline.CORE.Product" SelectMethod="GetProducts">
                <EmptyDataTemplate>
                    <table style="border: thin solid #000000">
                        <tr>
                            <td>No hay productos a mostrar</td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <EmptyItemTemplate>
                    <td />
                </EmptyItemTemplate>
                <GroupTemplate>
                    <tr id="itemPlaceholderContainer" runat="server">
                        <td id="itemPlaceholder" runat="server"></td>
                    </tr>
                </GroupTemplate>
                <ItemTemplate>
                    <td runat="server">
                        <table style="padding: 10px; margin: 10px; border: 1px solid #C0C0C0;">
                            <tr>
                                <td>
                                    <a href="ProductDetails?product=<%#:Item.Id%>">
                                        <image src='/<%#:Item.PathFile%>'
                                            width="200" height="200" border="1" />
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="center">
                                        <h5>
                                            <a href="ProductDetails?product=<%#:Item.Id%>">
                                                <%#:Item.Name%>
                                            </a>
                                        </h5>
                                        <span>
                                            <h5>
                                                <b>Precio: </b><%#:String.Format("{0:c}", Item.Price)%>
                                            </h5>
                                        </span>

                                        <a href="/AddToCart?productID=<%#:Item.Id %>" class="btn btn-success">
                                            Añadir                  
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                        </p>
                    </td>
                </ItemTemplate>
                <LayoutTemplate>
                    <table style="width: 100%;">
                        <tbody>
                            <tr>
                                <td>
                                    <table id="groupPlaceholderContainer" runat="server" style="width: 100%">
                                        <tr id="groupPlaceholder"></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr></tr>
                        </tbody>
                    </table>
                </LayoutTemplate>
            </asp:ListView>
        </div>
    </section>
</asp:Content>
