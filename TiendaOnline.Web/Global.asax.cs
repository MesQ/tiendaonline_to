﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using TiendaOnline.Application;

namespace TiendaOnline.Web
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Crea un nuevo rol y usuario
            RoleManager roleManager = new RoleManager();
            roleManager.AddUserAndRole();

            // Añade las rutas personalizadas
            RegisterCustomRoutes(RouteTable.Routes);
        }

        /// <summary>
        /// Metodo que registra las nuevas rutas personalizadas
        /// </summary>
        /// <param name="routes">rutas</param>
        void RegisterCustomRoutes(RouteCollection routes)
        {
            routes.MapPageRoute(
                "ProductsByCategoryRoute",
                "Category/{categoryName}",
                "~/ProductList.aspx"
            );
            routes.MapPageRoute(
                "ProductByNameRoute",
                "Product/{productName}",
                "~/ProductDetails.aspx"
            );
        }
    }
}