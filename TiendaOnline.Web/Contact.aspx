﻿<%@ Page Title="Contacto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="TiendaOnline.Web.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    
        <h3>Pongase en contacto con nosotros</h3>
        <br />
        <br />
        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="Nombre:"></asp:Label><br />
            <asp:TextBox ID="txtName" CssClass="form-control input-md" runat="server"></asp:TextBox><br />
        </div>

        <div class="form-group">
            <asp:Label ID="Label2" runat="server" Text="Email:"></asp:Label><br />
            <asp:TextBox ID="txtEmail" CssClass="form-control input-md" runat="server"></asp:TextBox><br />
        </div>

        <div class="form-group">
            <asp:Label ID="Label3" runat="server" Text="Asunto:"></asp:Label><br />
            <asp:TextBox ID="txtSubject" CssClass="form-control input-md" runat="server"></asp:TextBox><br />
        </div>

        <div class="form-group">
            <asp:Label ID="Label4" runat="server" Text="Mensaje:"></asp:Label><br />
            <asp:TextBox ID="txtMessage" CssClass="form-control input-md" TextMode="multiline" runat="server"></asp:TextBox><br />
        </div>

        <div class="form-group">
            <asp:Button ID="submit" CssClass="btn btn-primary" runat="server" Text="Enviar" OnClick="submit_Click" />
        </div>
</asp:Content>
