﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.DAL;
using TiendaOnline.Web.Models;

namespace TiendaOnline.Web
{
    public partial class Cart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            decimal total = GetTotal();
            decimal taxes = GetTaxes();
            //si total es mayor a 0 indicamos el total del pedido y los impuesto, si no el carrito esta vacio
            if (total > 0)
            {
                lblTotalPrice.Text = total.ToString() + "€";
                lblTax.Text = taxes.ToString() + "€";
            }
            else
            {
                lblTotalPrice.Text = "El carrito esta vacio";
                lblTax.Text = "El carrito esta vacio";
            }
        }

        /// <summary>
        /// Metodo que al hacer click sobre el boton
        /// redirige al Checkout
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCheckout_Click(object sender, EventArgs e)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            CartManager manager = new CartManager(context);
            string session = manager.GetCartId();
            var query = manager.Context.Carts;
            string client = query.Where(c => c.SessionId == session).Select(p => p.Client_Id).FirstOrDefault();
            //miramos que el usuario este autenticado
            if(User.Identity.IsAuthenticated)
            {
                //revisamos que el usuario haya añadido algo al carrito
                decimal total = GetTotal();
                if(total > 0)
                {
                    Response.Redirect("/Checkout/Default");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('La cesta esta vacia, no hay nada que comprar');", true);
                }
                    
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Debes haber iniciado sesión antes de comprar!'); setTimeout(function(){window.location.href ='/Account/Login'});", true);
            }

        }

        /// <summary>
        /// Metodo que obtiene todos los productos 
        /// del carrito de un usuario
        /// </summary>
        /// <returns>productos</returns>
        public List<TiendaOnline.CORE.Cart> GetCartItems()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            CartManager manager = new CartManager(context);
            return manager.GetCartItems();
        }

        /// <summary>
        /// Metodo que obtiene el precio total
        /// de todos los productos del carrito
        /// </summary>
        /// <returns>precio total</returns>
        public decimal GetTotal()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            CartManager manager = new CartManager(context);
            return manager.GetTotal();
        }


        /// <summary>
        /// Metodo que obtiene los impuestos del precio total
        /// </summary>
        /// <returns>precio impuestos</returns>
        public decimal GetTaxes()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            CartManager manager = new CartManager(context);
            return manager.GetTaxes();
        }

        /// <summary>
        /// Metodo que actualiza la cantidad del producto puesto dentro del textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grv_cart_RowCommand(Object sender, GridViewCommandEventArgs e)
        {
            //cogemos el numero de fila y el id mediante el command argument del boton actualizar
            string[] arg = e.CommandArgument.ToString().Split(';');
            string fila = arg[0].ToString();
            string id = arg[1].ToString();
            GridViewRow rowToUpdate = CartList.Rows[Convert.ToInt32(fila)];
            TextBox textQuantity = (TextBox)rowToUpdate.FindControl("PurchaseQuantity");
            int currentQuantity = Int32.Parse(Regex.Replace(textQuantity.Text, @"[^\d]", ""));

                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    con.Open();
                    // actualiza la cantidad del producto 
                    string query = string.Format("UPDATE Carts SET Quantity = '{0}' WHERE id = '{1}'", currentQuantity, id);
                    SqlCommand command = new SqlCommand(query, con);
                    command.ExecuteNonQuery();
                con.Close();
            }
                Response.Redirect("Cart");    
        }

    }
}


