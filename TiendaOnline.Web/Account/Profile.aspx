﻿<%@ Page Title="Mi Perfil" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="TiendaOnline.Web.Account.Profile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <h3>Mi Perfil</h3>
        <asp:HiddenField ID="txtId" runat="server" />
        <div class="form-horizontal">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />
        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="Nombre:"></asp:Label><br />
            <asp:TextBox ID="txtName" CssClass="form-control input-md" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator
                id="reqName"
                ErrorMessage="El nombre es obligatorio"
                ControlToValidate="txtName"
                Runat="server" CssClass="alert alert-error" />
        </div>

        <div class="form-group">
            <asp:Label ID="Label4" runat="server" Text="DNI:"></asp:Label><br />
            <asp:TextBox ID="txtDNI" CssClass="form-control input-md" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator
                id="RequiredFieldValidator6"
                ErrorMessage="El DNI es obligatorio"
                ControlToValidate="txtDNI"
                Runat="server" CssClass="alert alert-error" />
        </div>

        <div class="form-group">
            <asp:Label ID="Label5" runat="server" Text="Teléfono:"></asp:Label><br />
            <asp:TextBox ID="txtPhone" CssClass="form-control input-md" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator
                id="RequiredFieldValidator3"
                ErrorMessage="El teléfono es obligatorio"
                ControlToValidate="txtPhone"
                Runat="server" CssClass="alert alert-error" />
        </div>

        <div class="form-group">
            <asp:Label ID="Label3" runat="server" Text="País:"></asp:Label><br />
            <asp:TextBox ID="txtCountry" CssClass="form-control input-md" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator
                id="RequiredFieldValidator2"
                ErrorMessage="El país es obligatorio"
                ControlToValidate="txtCountry"
                Runat="server" CssClass="alert alert-error" />
        </div>

            <div class="form-group">
            <asp:Label ID="Label2" runat="server" Text="Provincia:"></asp:Label><br />
            <asp:TextBox ID="txtProvince" CssClass="form-control input-md" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator
                id="RequiredFieldValidator1"
                ErrorMessage="La provincia es obligatoria"
                ControlToValidate="txtProvince"
                Runat="server" CssClass="alert alert-error" />
        </div>

            <div class="form-group">
            <asp:Label ID="Label6" runat="server" Text="Ciudad:"></asp:Label><br />
            <asp:TextBox ID="txtCity" CssClass="form-control input-md" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator
                id="RequiredFieldValidator4"
                ErrorMessage="La ciudad es obligatoria"
                ControlToValidate="txtCity"
                Runat="server" CssClass="alert alert-error" />
        </div>

            <div class="form-group">
            <asp:Label ID="Label7" runat="server" Text="Calle:"></asp:Label><br />
            <asp:TextBox ID="txtStreet" CssClass="form-control input-md" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator
                id="RequiredFieldValidator5"
                ErrorMessage="La calle es obligatoria"
                ControlToValidate="txtStreet"
                Runat="server" CssClass="alert alert-error" />
        </div>

           <div class="form-group">
            <asp:Label ID="Label8" runat="server" Text="Número, portal, escalera, puerta...:"></asp:Label><br />
            <asp:TextBox ID="txtStreetDetails" CssClass="form-control input-md" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator
                id="RequiredFieldValidator7"
                ErrorMessage="La calle es obligatoria"
                ControlToValidate="txtStreetDetails"
                Runat="server" CssClass="alert alert-error" />
        </div>


        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary" runat="server" Text="Guardar" OnClick="btnSubmit_Click" />
            <a href="Manage"                
                <button  id="VolverBtn" class="btn btn-danger">Volver</button>
            </a>
        </div>
        </div>
        
</asp:Content>
