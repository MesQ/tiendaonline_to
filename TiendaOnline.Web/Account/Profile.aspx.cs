﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using TiendaOnline.DAL;
using TiendaOnline.Application;

namespace TiendaOnline.Web.Account
{
    public partial class Profile : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        ProfileManager manager = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            manager = new ProfileManager(context);

            var id = manager.GetByClientId(User.Identity.GetUserId());
            var profile = manager.GetById(id);
            if (profile != null)
            {
                if (!IsPostBack)
                {
                    LoadProfile(profile);
                }
                
            }

        }

        /// <summary>
        /// Metodo que carga los datos en los campos
        /// </summary>
        /// <param name="profile">perfil</param>
        private void LoadProfile(TiendaOnline.CORE.Profile profile)
        {
            txtName.Text = profile.Name;
            txtDNI.Text = profile.DNI;
            txtPhone.Text = profile.Phone;
            txtCountry.Text = profile.Country;
            txtProvince.Text = profile.Province;
            txtCity.Text = profile.City;
            txtStreet.Text = profile.Street;
            txtStreetDetails.Text = profile.StreetDetail;
        }

        /// <summary>
        /// Metodo que al hacer click sobre el botn Guardar 
        /// añade o actualiza los datos del perfil
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var id = manager.GetByClientId(User.Identity.GetUserId());
                var profile = manager.GetById(id);

                //si el perfil no es nulo actualiza los datos, si no crea uno nuevo
                if (profile != null)
                {
                    TiendaOnline.CORE.Profile userProfile = new TiendaOnline.CORE.Profile
                    {
                        Id = profile.Id,
                        Name = txtName.Text,
                        DNI = txtDNI.Text,
                        Phone = txtPhone.Text,
                        Country = txtCountry.Text,
                        Province = txtProvince.Text,
                        City = txtCity.Text,
                        Street = txtStreet.Text,
                        StreetDetail = txtStreetDetails.Text,
                        Client_Id = User.Identity.GetUserId()
                    };
                    manager.Update(userProfile);
                    manager.Context.SaveChanges();
                    //saca aviso de que se ha editado correctamente y devuelve a la pagina principal
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Perfil actualizado correctamente!');", true);
                }
                else
                {
                    TiendaOnline.CORE.Profile userProfile = new TiendaOnline.CORE.Profile
                    {
                        Name = txtName.Text,
                        DNI = txtDNI.Text,
                        Phone = txtPhone.Text,
                        Country = txtCountry.Text,
                        Province = txtProvince.Text,
                        City = txtCity.Text,
                        Street = txtStreet.Text,
                        StreetDetail = txtStreetDetails.Text,
                        Client_Id = User.Identity.GetUserId()
                    };
                    manager.Add(userProfile);
                    context.SaveChanges();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Perfil añadido correctamente!');", true);
                }
            }
            catch (Exception ex)
            {
                //Muestra error si no se ejecuta el try
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al modificar, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                //Escribe el log en el fichero
                string path = Server.MapPath("~/Logs/Errors.txt");
                manager.ErrorLog(path, ex.Message);
            }

        }
    }
}