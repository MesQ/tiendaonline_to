﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.DAL;

namespace TiendaOnline.Web.Account
{
    public partial class ViewOrder : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        OrderManager orderManager = null;
        ProfileManager profileManager = null;
        OrderProductManager orderProManager = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            orderManager = new OrderManager(context);
            profileManager = new ProfileManager(context);

            //obtiene id de la url
            int id = 0;
            if (Request.QueryString["id"] != null)
            {
                if (int.TryParse(Request.QueryString["id"], out id))
                {
                    var order = orderManager.GetById(id);
                    if (order != null)
                    {
                        string clientId = order.Client_Id.ToString();
                        var userId = profileManager.GetByClientId(clientId);
                        var profile = profileManager.GetById(userId);
                        if (!Page.IsPostBack)
                            LoadProfile(profile);
                    }
                }
            }

        }

        /// <summary>
        /// Metodo que carga los datos en los campos
        /// </summary>
        /// <param name="profile">perfil</param>
        private void LoadProfile(TiendaOnline.CORE.Profile profile)
        {
            txtName.Text = profile.Name;
            txtDNI.Text = profile.DNI;
            txtPhone.Text = profile.Phone;
            txtCountry.Text = profile.Country;
            txtProvince.Text = profile.Province;
            txtCity.Text = profile.City;
            txtStreet.Text = profile.Street;
            txtStreetDetails.Text = profile.StreetDetail;
        }

        /// <summary>
        /// Metodo que obtiene todos los productos 
        /// de un pedido
        /// </summary>
        /// <returns>productos</returns>
        public List<TiendaOnline.CORE.OrderProduct> GetOrderProducts()
        {
            context = new ApplicationDbContext();
            orderProManager = new OrderProductManager(context);

            int id = int.Parse(Request.QueryString["id"]);
            var query = orderProManager.Context.OrderProducts;
            return query.Where(c => c.Order_Id == id).ToList();

        }
    }
}
