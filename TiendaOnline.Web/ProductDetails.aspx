﻿<%@ Page Title="Producto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductDetails.aspx.cs" Inherits="TiendaOnline.Web.ProductDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" href="/Content/lightbox.css" type="text/css" media="screen" />
    <div align="center"><h1><asp:Label ID="lblStock" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label></h1></div>
    
        <asp:FormView ID="productDetail" runat="server" ItemType="TiendaOnline.CORE.Product" SelectMethod ="GetProduct" RenderOuterTable="false">
        <ItemTemplate>
            <div>
                <h1><%#:Item.Name %></h1>
            </div>
            <br />
            <table>
                <tr>
                    <td>
                        <img src="/<%#:Item.PathFile %>" style="border: 1px solid #C0C0C0; height:340px; width:340px"  alt="<%#:Item.Name %>"/>
                    </td>
                    <td>&nbsp;</td>  
                    <td>&nbsp;</td> 
                    <td>&nbsp;</td>  
                    <td>&nbsp;</td> 
                    <td style="vertical-align: top; text-align:left;">
                        <b>Descripción:</b><br /><%#:Item.Description %><br /><br />
                        <b>Categoria:</b> <%#:Item.Category.Name %><br /><br />
                        <b>Impuesto:</b> <%#:Item.Tax.Name %><br /><br />
                        <span><b>Precio:</b>&nbsp;<%#: String.Format("{0:c}", Item.Price) %></span><br /><br />
                        <a id="btnAdd" href="/AddToCart?productID=<%#:Item.Id %>&quantity=" class="btn btn-success">Añadir</a>
                    </td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:FormView>
    
            <script type="text/javascript" src="/Scripts/prototype.js"></script>
            <script type="text/javascript" src="/Scripts/scriptaculous.js?load=effects"></script>
            <script type="text/javascript" src="/Scripts/lightbox.js"></script>
            <table>
                <tr>
                <td colspan="2">
                    <asp:DataList ID="dlImages" runat="server" RepeatColumns="3" CellPadding="5" Width="340px">
                    <ItemTemplate>
                    <a id="imageLink" href='<%# Eval("PathFile","/{0}") %>'  rel="lightbox[Brussels]" runat="server" >
                    <asp:Image ID="Image1" ImageUrl='<%# Bind("PathFile","/{0}") %>' runat="server" Width="112" Height="84" />
                    </a> 
                    </ItemTemplate>
                    <ItemStyle BorderColor="#C0C0C0" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Center"
                    VerticalAlign="Bottom" />
                    </asp:DataList>
                    </td>
                </tr>
            </table>
    <h3>Manuales del Producto:</h3>
    <asp:GridView ID="documentList" runat="server" AutoGenerateColumns="False" ShowFooter="True" GridLines="Vertical" CellPadding="4"
        ItemType="TiendaOnline.CORE.Document" SelectMethod="GetDocuments" 
        CssClass="table table-striped table-bordered" >   
            <EmptyDataTemplate>

    <div>

    No hay documentos disponibles.

    </div>

    </EmptyDataTemplate> 
        <Columns>       
        <asp:BoundField DataField="Name" HeaderText="Nombre" />            
        <asp:TemplateField   HeaderText="Enlace">            
                <ItemTemplate>                   
                    <a href="/<%#:Item.PathFile %>">Enlace al archivo</a>
                </ItemTemplate>        
        </asp:TemplateField>    
        </Columns>    
    </asp:GridView>

</asp:Content>
