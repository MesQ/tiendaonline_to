﻿<%@ Page Title="Error" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="TiendaOnline.Web.Checkout.Error" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div align="center">
        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/error.png" />
        <h1>Oops!</h1>
        <h2>Se ha producido un error al hacer el pedido</h2>
        <h3>Revise si su metodo de pago es corecto</h3>
        <div class="error-details">
            Sentimos las molestias, si el error persiste puedes ponerte en contacto con nosotros.
               
        </div>
        <br />
        <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/Products.aspx" CssClass="btn btn-primary">Volver a Productos</asp:LinkButton>&nbsp &nbsp
    <asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="~/Contact.aspx" CssClass="btn btn-success">Contacto</asp:LinkButton>
    </div>
</asp:Content>
