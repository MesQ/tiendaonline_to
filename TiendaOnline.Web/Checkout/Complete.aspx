﻿<%@ Page Title="Completado" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Complete.aspx.cs" Inherits="TiendaOnline.Web.Checkout.Complete" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <div align="center">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/success.png" />
        <h1>Pedido completado correctamente!</h1>
        <div class="error-details">
            Puedes comprobar tu pedido desde Mi Cuenta.
               
        </div>
        <br />
        <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/Account/Orders.aspx" CssClass="btn btn-primary">Mis Pedidos</asp:LinkButton>
    </div>
</asp:Content>
