﻿<%@ Page Title="Pago TPV" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TPV.aspx.cs" Inherits="TiendaOnline.Web.Checkout.TPV" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
      <form class="form-horizontal span6">
        <fieldset>
          <legend>Pago TPV</legend>
       
          <div class="form-group">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />
            <div class="col-md-6">
                <asp:Label ID="Label1" runat="server" Text="Nombre"></asp:Label>
                <asp:TextBox ID="txtCardName" CssClass="form-control input-md" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator
                id="reqName"
                ErrorMessage="El nombre es obligatorio"
                ControlToValidate="txtCardName"
                Runat="server" CssClass="alert alert-error" />
            </div>
              <div class="col-md-6">
                  <asp:Label ID="Label6" runat="server" Text="Número Tarjeta"></asp:Label>
                <asp:TextBox ID="txtCardNumber" CssClass="form-control input-md" runat="server"></asp:TextBox>
                  <asp:RequiredFieldValidator
                id="RequiredFieldValidator1"
                ErrorMessage="El número de tarjeta es obligatorio"
                ControlToValidate="txtCardNumber"
                Runat="server" CssClass="alert alert-error" />
              </div>
          </div><br />
            
            <div class="form-group">
            
            <div class="col-md-4">
                <asp:Label ID="Label2" runat="server" Text="Mes"></asp:Label>
                <asp:TextBox ID="txtMonth" CssClass="form-control input-md" runat="server">12</asp:TextBox>
                <asp:RequiredFieldValidator
                id="RequiredFieldValidator2"
                ErrorMessage="El mes es obligatorio"
                ControlToValidate="txtMonth"
                Runat="server" CssClass="alert alert-error" />
            </div>
              <div class="col-md-4">
                  <asp:Label ID="Label3" runat="server" Text="Año"></asp:Label>
                <asp:TextBox ID="txtYear" CssClass="form-control input-md" runat="server">2018</asp:TextBox>
                <asp:RequiredFieldValidator
                id="RequiredFieldValidator3"
                ErrorMessage="El año de la tarjeta es obligatorio"
                ControlToValidate="txtYear"
                Runat="server" CssClass="alert alert-error" />
              </div>
               <div class="col-md-4">
                  <asp:Label ID="Label4" runat="server" Text="CCC"></asp:Label>
                <asp:TextBox ID="txtCCC" CssClass="form-control input-md" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator
                id="RequiredFieldValidator4"
                ErrorMessage="El número CCC es obligatorio"
                ControlToValidate="txtCCC"
                Runat="server" CssClass="alert alert-error" />
              </div>
          </div>
       
   

        </fieldset>
      </form>
             <br /><div align="center">
          <div class="form-group">
            <asp:Button ID="btnPay" runat="server" Text="Pagar" CssClass="btn btn-success" OnClick="btnPay_Click" />&nbsp&nbsp
            <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/Checkout/Default.aspx" CssClass="btn btn-danger">Volver</asp:LinkButton>
          </div>
                </div>
    <br /><div align="center">
    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/redsys-visa.png" />
        </div>
</asp:Content>
