﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.CORE;
using TiendaOnline.DAL;

namespace TiendaOnline.Web.Checkout
{
    public partial class Default : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        ProfileManager profileManager = null;
        CartManager cartManager = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            profileManager = new ProfileManager(context);
            cartManager = new CartManager(context);
            var id = profileManager.GetByClientId(User.Identity.GetUserId());
            var profile = profileManager.GetById(id);
            if (profile != null)
            {
                LoadProfile(profile);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('No tenemos información sobre tí. Porfavor rellena los datos antes de comprar'); setTimeout(function(){window.location.href ='/Account/Profile'});", true);
            }

            if (!IsPostBack)
            {
                ddlPayment.DataSource = Enum.GetValues(typeof(PaymentMethod));
                ddlPayment.DataBind();
            }

            lblTotalPrice.Text = cartManager.GetTotal().ToString();
            lblTax.Text = cartManager.GetTaxes().ToString();
        }

        /// <summary>
        /// Metodo que carga los del perfil en mis datos
        /// </summary>
        /// <param name="profile">perfil</param>
        private void LoadProfile(TiendaOnline.CORE.Profile profile)
        {
            txtName.Text = profile.Name;
            txtDNI.Text = profile.DNI;
            txtPhone.Text = profile.Phone;
            txtCountry.Text = profile.Country;
            txtProvince.Text = profile.Province;
            txtCity.Text = profile.City;
            txtStreet.Text = profile.Street;
            txtStreetDetails.Text = profile.StreetDetail;
        }

        /// <summary>
        /// Metodo que obtiene todos los productos 
        /// del carrito de un usuario
        /// </summary>
        /// <returns>productos</returns>
        public List<TiendaOnline.CORE.Cart> GetCartItems()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            CartManager manager = new CartManager(context);
            string session = manager.GetCartId();
            var query = manager.Context.Carts;
            return query.Where(c => c.SessionId == session).ToList();
        }

        /// <summary>
        /// Metodo que obtiene el precio total
        /// de todos los productos del carrito
        /// </summary>
        /// <returns>precio total</returns>
        public decimal GetTotal()
        {
           return cartManager.GetTotal();
        }


        /// <summary>
        /// Metodo que obtiene los impuestos del precio total
        /// </summary>
        /// <returns>precio impuestos</returns>
        public decimal GetTaxes()
        {
            return cartManager.GetTaxes();
        }

        /// <summary>
        /// Metodo que borra todos los productos 
        /// del carrito de un usuario
        /// </summary>
        public void EmptyCart()
        {
            cartManager.EmptyCart();
        }

        /// <summary>
        /// Metodo que cancela el pedido 
        /// eliminando los productos del carrito
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            cartManager.EmptyCart();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Pedido cancelado correctamente'); setTimeout(function(){window.location.href ='/Products'});", true);

        }

        /// <summary>
        /// Metodo que al hacer click sobre el boton comprar
        /// empieza el proceso de compra del pedido
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCheckout_Click(object sender, EventArgs e)
        {
            //dependiendo del metodo de pago elegido redirige a la opcion elegida
            switch (ddlPayment.SelectedValue)
            {
                case "Efectivo":
                    CashPay();
                    Response.Redirect("Complete");
                    break;
                case "Tarjeta":
                    Response.Redirect("TPV");
                    break;
                case "PayPal":
                    Response.Redirect("PayPal");
                    break;
                case "Transferencia":
                    Response.Redirect("Transfer");
                    break;
            }
        }

        /// <summary>
        /// Metodo que al comprar en efectivo realiza la compra de los productos
        /// y calcula el stock actual
        /// </summary>
        public void CashPay()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            ProfileManager profManager = new ProfileManager(context);
            OrderManager orderManager = new OrderManager(context);
            OrderProductManager orderProManager = new OrderProductManager(context);
            UserManager userManager = new UserManager(context);
            try
            {
                //Creamos el pedido
                var id = profManager.GetByClientId(User.Identity.GetUserId());
                var profile = profManager.GetById(id);
                TiendaOnline.CORE.Order order = new TiendaOnline.CORE.Order
                {
                    Client_Id = User.Identity.GetUserId(),
                    Status = OrderStatus.Nuevo,
                    PurchaseDate = DateTime.Now,
                    Paid = false,
                    PaymentMethod = PaymentMethod.Efectivo,
                    Shipped = false,
                    Tracking = null,
                    Profile_Id = profile.Id,
                    TotalPrice = GetTotal()

                };

                orderManager.Add(order);
                orderManager.Context.SaveChanges();

                //Añadimos los productos a la tabla OrderProducts
                int counter = 0;
                var products = GetCartItems();
                foreach (var product in products)
                {
                    TiendaOnline.CORE.OrderProduct orderProduct = new TiendaOnline.CORE.OrderProduct
                    {
                        Order_Id = order.Id,
                        Product_Id = product.Product_Id,
                        Quantity = product.Quantity,
                        ProductName = product.Product.Name,
                        Price = product.Price,
                        Total = product.Quantity * product.Price
                    };

                    //obtenemos el stock del product y restamos la cantidad comprada
                    counter = counter + orderProduct.Quantity;
                    var consulta = orderManager.Context.Products;
                    int stock = consulta.Where(c => c.Id == orderProduct.Product_Id).Select(c => c.Stock).FirstOrDefault();
                    int result = stock - counter;

                    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        con.Open();
                        // Update the cart quantity if the id matches with the grid view data source
                        string query = string.Format("UPDATE Products SET Stock = '{0}' WHERE id = '{1}'", result, orderProduct.Product_Id);
                        SqlCommand command = new SqlCommand(query, con);
                        command.ExecuteNonQuery();
                        con.Close();
                    }

                    orderProManager.Add(orderProduct);
                    orderProManager.Context.SaveChanges();
                }

                EmptyCart();

                //Envio de emails
                //email para cliente
                MailMessage mail = new MailMessage();
                System.Net.Mail.SmtpClient SmtpServer = new System.Net.Mail.SmtpClient("smtp.live.com");
                mail.From = new System.Net.Mail.MailAddress("tiendaonline.to@hotmail.com");
                string email = userManager.GetByUserId(HttpContext.Current.User.Identity.GetUserId()).Email;
                mail.To.Add(email);
                mail.Subject = "Nuevo Pedido";
                mail.Body = "has realizado un nuevo pedido en nuestra web puedes seguir los detalles desde mi cuenta.";
                //email para tienda
                MailMessage mail2 = new MailMessage();
                mail2.From = new System.Net.Mail.MailAddress("tiendaonline.to@hotmail.com");
                mail2.To.Add("tiendaonline.to@hotmail.com");
                mail2.Subject = "Nuevo Pedido";
                mail2.Body = "Se ha recibido un nuevo pedido en la tienda.";
                //envio de los 2 emails
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("tiendaonline.to@hotmail.com", "1A2B3C4d5e");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
                SmtpServer.Send(mail2);
                //redirige a la apgina de completado
                Response.Redirect("Complete");

            }
            catch (Exception ex)
            {
                
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al añadir, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                //Ecribimos el error en un fichero
                string path = Server.MapPath("~/Logs/Errors.txt");
                orderProManager.ErrorLog(path, ex.Message);
            }
}

    }
}
    

