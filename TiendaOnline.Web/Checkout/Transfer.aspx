﻿<%@ Page Title="Transferencia" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Transfer.aspx.cs" Inherits="TiendaOnline.Web.Checkout.Transfer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
        <div align="center">
        <h1>Pago por Transferencia Bancaria</h1>
        <div class="error-details">
            El pago se debe realizar a la siguiente cuenta: <br />
            <b>ES65 0000 0000 0000 0000 0000 </b>
               
        </div>
        <br />
            <asp:Button ID="btnPay" runat="server" Text="Aceptar" CssClass="btn btn-success" OnClick="btnPay_Click" />
    </div>
</asp:Content>
