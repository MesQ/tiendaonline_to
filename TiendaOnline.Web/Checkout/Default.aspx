﻿<%@ Page Title="Comprar" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TiendaOnline.Web.Checkout.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h3>Mis Datos</h3>
    <div class="form-horizontal">
    <div class="form-group">
        <div class="col-md-4">
            <asp:Label ID="Label1" runat="server" Text="Nombre:"></asp:Label>
            <asp:TextBox ID="txtName" CssClass="form-control input-md" runat="server" Enabled="False"></asp:TextBox>
        </div>
        <div class="col-md-4">
            <asp:Label ID="Label4" runat="server" Text="DNI:"></asp:Label><br />
            <asp:TextBox ID="txtDNI" CssClass="form-control input-md" runat="server" Enabled="False"></asp:TextBox>
        </div>
        <div class="col-md-4">
            <asp:Label ID="Label5" runat="server" Text="Teléfono:"></asp:Label><br />
            <asp:TextBox ID="txtPhone" CssClass="form-control input-md" runat="server" Enabled="False"></asp:TextBox>
        </div>
    </div>
    

    <div class="form-group">
        <div class="col-md-4">
            <asp:Label ID="Label3" runat="server" Text="País:"></asp:Label><br />
            <asp:TextBox ID="txtCountry" CssClass="form-control input-md" runat="server" Enabled="False"></asp:TextBox>
        </div>
        <div class="col-md-4">
            <asp:Label ID="Label2" runat="server" Text="Provincia:"></asp:Label><br />
            <asp:TextBox ID="txtProvince" CssClass="form-control input-md" runat="server" Enabled="False"></asp:TextBox>
        </div>
        <div class="col-md-4">
            <asp:Label ID="Label6" runat="server" Text="Ciudad:"></asp:Label><br />
            <asp:TextBox ID="txtCity" CssClass="form-control input-md" runat="server" Enabled="False"></asp:TextBox>
        </div>
    </div>


    <div class="form-group">
        <div class="col-md-4">
            <asp:Label ID="Label7" runat="server" Text="Calle:"></asp:Label><br />
            <asp:TextBox ID="txtStreet" CssClass="form-control input-md" runat="server" Enabled="False"></asp:TextBox>
        </div>
        <div class="col-md-4">
            <asp:Label ID="Label8" runat="server" Text="Número, portal, escalera, puerta...:"></asp:Label><br />
            <asp:TextBox ID="txtStreetDetails" CssClass="form-control input-md" runat="server" Enabled="False"></asp:TextBox>
        </div>
    </div>
    </div>

    <h3>Metodo de Pago</h3>
    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-md-4">
                <asp:Label ID="lblPayment" runat="server" Text="Elija el metodo de pago:"></asp:Label>
                <asp:DropDownList ID="ddlPayment" runat="server" CssClass="form-control input-md"></asp:DropDownList>
            </div>
        </div>
    </div>

    <h3>Pedido</h3>
        <asp:GridView ID="CartList" runat="server" AutoGenerateColumns="False" GridLines="Vertical" CellPadding="4"
        ItemType="TiendaOnline.CORE.Cart" SelectMethod="GetCartItems" 
        CssClass="table table-striped table-bordered" AllowSorting="True" >   
        <Columns>
                   
        <asp:BoundField DataField="Product_Id" HeaderText="Product_Id" SortExpression="Product_Id" Visible="False" />        
        <asp:BoundField DataField="Id" Visible="False" />
        <asp:BoundField DataField="Product.Name" HeaderText="Nombre" />        
        <asp:BoundField DataField="Product.Price" HeaderText="Precio (unidad)" DataFormatString="{0:c}"/>
            <asp:BoundField DataField="Product.Tax.Name" HeaderText="Impuesto" DataFormatString="{0:c}"/>  
        <asp:TemplateField   HeaderText="Cantidad">            
                <ItemTemplate>
                    <<%#: Item.Quantity %>> 
                </ItemTemplate>        
        </asp:TemplateField>    
        <asp:TemplateField HeaderText="Precio Total">            
                <ItemTemplate>
                    <%#: String.Format("{0:c}", ((Convert.ToDouble(Item.Quantity)) *  Convert.ToDouble(Item.Product.Price)))%>
                </ItemTemplate>        
        </asp:TemplateField>    
        </Columns>    
    </asp:GridView>
    <div>
        <p></p>
        <strong>
            <asp:Label ID="lblTaxName" runat="server" Text="Total Impuestos: "></asp:Label>
            <asp:Label ID="lblTax" runat="server" EnableViewState="False" ForeColor="Blue"></asp:Label>
        </strong> <br />
                <strong>
            <asp:Label ID="Label9" runat="server" Text="Total Pedido: "></asp:Label>
            <asp:Label ID="lblTotalPrice" runat="server" EnableViewState="false" ForeColor="Blue"></asp:Label>
        </strong> 
    </div>
    <table> 
    <tr>
      <td>
          <asp:Button ID="btnCheckout" runat="server" CssClass="btn btn-success" Text="Comprar" OnClick="btnCheckout_Click"/>&nbsp&nbsp
          <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-danger" Text="Cancelar" OnClick="btnCancel_Click" />
      </td>
    </tr>
    </table>
</asp:Content>
