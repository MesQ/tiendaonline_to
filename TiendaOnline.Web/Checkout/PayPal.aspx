﻿<%@ Page Title="PayPal" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PayPal.aspx.cs" Inherits="TiendaOnline.Web.Checkout.PayPal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div align="center">
        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/paypal-logo.png" />
                <div class="form-horizontal">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />
        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="Usuario:"></asp:Label><br />
            <asp:TextBox ID="txtUser" CssClass="form-control input-md" runat="server" TextMode="Email"></asp:TextBox>
            <asp:RequiredFieldValidator
                id="reqName"
                ErrorMessage="El usuario es obligatorio"
                ControlToValidate="txtUser"
                Runat="server" CssClass="alert alert-error" />
        </div>

        <div class="form-group">
            <asp:Label ID="Label4" runat="server" Text="Password:"></asp:Label><br />
            <asp:TextBox ID="txtPassword" CssClass="form-control input-md" runat="server" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator
                id="RequiredFieldValidator6"
                ErrorMessage="La password es obligatoria"
                ControlToValidate="txtPassword"
                Runat="server" CssClass="alert alert-error" />
        </div>
                    <div class="form-group">
                    <asp:Button ID="btnPay" runat="server" Text="Pagar" CssClass="btn btn-success" OnClick="btnPay_Click" />
                        </div>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/paypal-protect.gif" />
    </div>
        </div>
</asp:Content>
