﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.CORE;
using TiendaOnline.Application;
using System.Web.ModelBinding;
using TiendaOnline.DAL;

namespace TiendaOnline.Web
{
    public partial class ProductList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Obtiene las categorias
        /// </summary>
        /// <returns>categorias</returns>
        public IQueryable<Category> GetCategories()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            CategoryManager manager = new CategoryManager(context);
            IQueryable<Category> query = manager.Context.Categories;
            return query;
        }

        /// <summary>
        /// Metodoq ue obtiene los productos
        /// </summary>
        /// <param name="Category_Id">id de categoria</param>
        /// <returns>productos</returns>
        public IQueryable<Product> GetProducts([QueryString("Id")] int? Category_Id)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager manager = new ProductManager(context);

            IQueryable<Product> query = manager.Context.Products;
            if (Category_Id.HasValue && Category_Id > 0)
            {
                query = query.Where(p => p.Category_Id == Category_Id);
            }
            return query;
        }
    }
}

