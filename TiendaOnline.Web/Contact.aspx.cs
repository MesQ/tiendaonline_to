﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.Application;
using TiendaOnline.DAL;

namespace TiendaOnline.Web
{
    public partial class Contact : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Metodo que al clickar sobre el boton envia un correo electronico a la tienda
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void submit_Click(object sender, EventArgs e)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager manager = new ProductManager(context);
            UserManager userManager = new UserManager(context);
            //Envio de emails
            //email para cliente
            MailMessage mail = new MailMessage();
            System.Net.Mail.SmtpClient SmtpServer = new System.Net.Mail.SmtpClient("smtp.live.com");
            mail.From = new System.Net.Mail.MailAddress("tiendaonline.to@hotmail.com");
            mail.To.Add("tiendaonline.to@hotmail.com");
            mail.ReplyTo = new System.Net.Mail.MailAddress(txtEmail.Text, txtName.Text);
            mail.Subject = txtSubject.Text;
            mail.Body = txtMessage.Text;
            try
            {
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("tiendaonline.to@hotmail.com", "1A2B3C4d5e");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Email enviado correctamente');", true);
            }
            catch (Exception exe)
            {
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al enviar el correo, si el error persiste contacte con el administrador",
                    IsValid = false
                };
                Page.Validators.Add(err);
                //Ecribimos el error en un fichero
                string path = Server.MapPath("~/Logs/Errors.txt");
                manager.ErrorLog(path, exe.Message);
            }

        }

    }
}
