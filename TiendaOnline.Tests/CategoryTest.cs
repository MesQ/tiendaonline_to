﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TiendaOnline.Application;
using TiendaOnline.CORE;
using TiendaOnline.DAL;
namespace TiendaOnline.Tests
{
    /// <summary>
    /// Test de la entida categoria
    /// </summary>
    [TestClass]
    public class CategoryTest
    {
        /// <summary>
        /// Metodo de añadir una categoria
        /// </summary>
        [TestMethod]
        public void AddTest()
        {
            //Arrange            
            ApplicationDbContext context = new ApplicationDbContext();
            CategoryManager manager = new CategoryManager(context);
            Category category = new Category { Id=1000, Name = "Hola" };

            //Act
            manager.Add(category);

            //Assert
            Assert.AreEqual(manager.GetById(1000), category);
        }

        /// <summary>
        /// Metodo de borrar categoria
        /// </summary>
        [TestMethod]
        public void RemoveTest()
        {
            Assert.IsTrue(true);
        }

        public void FixEfProviderServicesProblem()
        {
            //The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            //for the 'System.Data.SqlClient' ADO.NET provider could not be loaded. 
            //Make sure the provider assembly is available to the running application. 
            //See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.

            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }
}
