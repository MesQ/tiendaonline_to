﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TiendaOnline.DAL;
using TiendaOnline.Application;

namespace TiendaOnline.Tests
{
    /// <summary>
    /// Summary description for OrderTest
    /// </summary>
    [TestClass]
    public class OrderTest
    {
        /// <summary>
        /// Summary description for OrderProduct
        /// </summary>
        [TestClass]
        public class OrderProduct
        {
            /// <summary>
            /// Metodo de añadir a orderproduct
            /// </summary>
            [TestMethod]
            public void AddTest()
            {
                //Arrange            
                ApplicationDbContext context = new ApplicationDbContext();
                OrderManager manager = new OrderManager(context);
                TiendaOnline.CORE.Order order = new TiendaOnline.CORE.Order
                {
                    Id = 10000,
                    Client_Id = "hola",
                    Paid = true,
                    PaymentMethod = CORE.PaymentMethod.PayPal,
                    Profile_Id = 1,
                    PurchaseDate = DateTime.Now,
                    SendDate = DateTime.Now,
                    Shipped = false,
                    Tracking = "111111",
                    Status = CORE.OrderStatus.Enviado,
                    TotalPrice = 1000
                };

                //Act
                manager.Add(order);

                //Assert
                Assert.AreEqual(manager.GetById(10000), order);
            }

            /// <summary>
            /// Metodo de borrar orderproduct
            /// </summary>
            [TestMethod]
            public void RemoveTest()
            {
                Assert.IsTrue(true);
            }

            public void FixEfProviderServicesProblem()
            {
                //The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
                //for the 'System.Data.SqlClient' ADO.NET provider could not be loaded. 
                //Make sure the provider assembly is available to the running application. 
                //See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.

                var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
            }
        }
    }
}
