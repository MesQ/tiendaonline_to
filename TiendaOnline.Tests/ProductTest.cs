﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TiendaOnline.DAL;
using TiendaOnline.Application;
using TiendaOnline.CORE;

namespace TiendaOnline.Tests
{
    /// <summary>
    /// Summary description for ProductTest
    /// </summary>
    [TestClass]
    public class ProductTest
    {
        /// <summary>
        /// Metodo de añadir un producto
        /// </summary>
        [TestMethod]
        public void AddTest()
        {
            //Arrange            
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager manager = new ProductManager(context);
            Product product = new Product { Id=1000, Name = "Hola", Category_Id = 1, Description = "hola", Price =10, Stock=5, Tax_Id=1,  };

            //Act
            manager.Add(product);

            //Assert
            Assert.AreEqual(manager.GetById(1000), product);
        }

        /// <summary>
        /// Metodo de borrar producto
        /// </summary>
        [TestMethod]
        public void RemoveTest()
        {
            Assert.IsTrue(true);
        }

        public void FixEfProviderServicesProblem()
        {
            //The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            //for the 'System.Data.SqlClient' ADO.NET provider could not be loaded. 
            //Make sure the provider assembly is available to the running application. 
            //See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.

            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }
}
