﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TiendaOnline.DAL;
using TiendaOnline.Application;
using TiendaOnline.CORE;

namespace TiendaOnline.Tests
{
    /// <summary>
    /// Summary description for CartTestcs
    /// </summary>
    [TestClass]
    public class CartTestcs
    {
        /// <summary>
        /// Metodo de añadir a carrito
        /// </summary>
        [TestMethod]
        public void AddTest()
        {
            //Arrange            
            ApplicationDbContext context = new ApplicationDbContext();
            CartManager manager = new CartManager(context);
            Cart cart = new Cart { Id=10000, Client_Id="hola", Date= DateTime.Now, Price=10, Quantity=1, Product_Id=1, SessionId="hola"};
           
            //Act
            manager.Add(cart);

            //Assert
            Assert.AreEqual(manager.GetById(10000), cart);
        }

        /// <summary>
        /// Metodo de borrar linea carrito
        /// </summary>
        [TestMethod]
        public void RemoveTest()
        {
            Assert.IsTrue(true);
        }

        public void FixEfProviderServicesProblem()
        {
            //The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            //for the 'System.Data.SqlClient' ADO.NET provider could not be loaded. 
            //Make sure the provider assembly is available to the running application. 
            //See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.

            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }
}
