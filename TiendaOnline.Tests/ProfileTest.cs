﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TiendaOnline.DAL;
using TiendaOnline.Application;
using TiendaOnline.CORE;

namespace TiendaOnline.Tests
{
    /// <summary>
    /// Summary description for UnitTest2
    /// </summary>
    [TestClass]
    public class ProfileTest
    {
        /// <summary>
        /// Metodo de añadir a orderproduct
        /// </summary>
        [TestMethod]
        public void AddTest()
        {
            //Arrange            
            ApplicationDbContext context = new ApplicationDbContext();
            ProfileManager manager = new ProfileManager(context);
            Profile profile = new Profile { Id = 10000, Client_Id="hola", City="hola", DNI= "hola" , Country= "hola", Name= "hola", Phone= "hola", Province= "hola", Street= "hola", StreetDetail= "hola" };

            //Act
            manager.Add(profile);

            //Assert
            Assert.AreEqual(manager.GetById(10000), profile);
        }

        /// <summary>
        /// Metodo de borrar orderproduct
        /// </summary>
        [TestMethod]
        public void RemoveTest()
        {
            Assert.IsTrue(true);
        }

        public void FixEfProviderServicesProblem()
        {
            //The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            //for the 'System.Data.SqlClient' ADO.NET provider could not be loaded. 
            //Make sure the provider assembly is available to the running application. 
            //See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.

            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }
}
