﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TiendaOnline.DAL;
using TiendaOnline.Application;
using TiendaOnline.CORE;

namespace TiendaOnline.Tests
{
    /// <summary>
    /// Summary description for DocumentTest
    /// </summary>
    [TestClass]
    public class DocumentTest
    {
        /// <summary>
        /// Metodo de añadir un documento
        /// </summary>
        [TestMethod]
        public void AddTest()
        {
            //Arrange            
            ApplicationDbContext context = new ApplicationDbContext();
            DocumentManager manager = new DocumentManager(context);
            Document document = new Document { Id=1000, Name = "Hola", PathFile = "/hola", Product_Id = 1 };

            //Act
            manager.Add(document);

            //Assert
            Assert.AreEqual(manager.GetById(1000), document);
        }

        /// <summary>
        /// Metodo de borrar documento
        /// </summary>
        [TestMethod]
        public void RemoveTest()
        {
            Assert.IsTrue(true);
        }

        public void FixEfProviderServicesProblem()
        {
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }
}
