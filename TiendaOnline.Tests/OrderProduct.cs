﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TiendaOnline.DAL;
using TiendaOnline.Application;
using TiendaOnline.CORE;

namespace TiendaOnline.Tests
{
    /// <summary>
    /// Summary description for OrderProduct
    /// </summary>
    [TestClass]
    public class OrderProduct
    {
        /// <summary>
        /// Metodo de añadir a orderproduct
        /// </summary>
        [TestMethod]
        public void AddTest()
        {
            //Arrange            
            ApplicationDbContext context = new ApplicationDbContext();
            OrderProductManager manager = new OrderProductManager(context);
            TiendaOnline.CORE.OrderProduct product = new TiendaOnline.CORE.OrderProduct {Id=10000, Order_Id=1, Price=1, ProductName="hola", Product_Id=1, Quantity=2, Total=2 };

            //Act
            manager.Add(product);

            //Assert
            Assert.AreEqual(manager.GetById(10000), product);
        }

        /// <summary>
        /// Metodo de borrar orderproduct
        /// </summary>
        [TestMethod]
        public void RemoveTest()
        {
            Assert.IsTrue(true);
        }

        public void FixEfProviderServicesProblem()
        {
            //The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            //for the 'System.Data.SqlClient' ADO.NET provider could not be loaded. 
            //Make sure the provider assembly is available to the running application. 
            //See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.

            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }
}
