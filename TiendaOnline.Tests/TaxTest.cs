﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TiendaOnline.DAL;
using TiendaOnline.Application;
using TiendaOnline.CORE;

namespace TiendaOnline.Tests
{
    /// <summary>
    /// Summary description for TaxTest
    /// </summary>
    [TestClass]
    public class TaxTest
    {
        /// <summary>
        /// Summary description for DocumentTest
        /// </summary>
        [TestClass]
        public class DocumentTest
        {
            /// <summary>
            /// Metodo de añadir un impuesto
            /// </summary>
            [TestMethod]
            public void AddTest()
            {
                //Arrange            
                ApplicationDbContext context = new ApplicationDbContext();
                TaxManager manager = new TaxManager(context);
                Tax tax = new Tax { Id=1000, Name = "Hola", Percentage = 10 };

                //Act
                manager.Add(tax);

                //Assert
                Assert.AreEqual(manager.GetById(1000), tax);
            }

            /// <summary>
            /// Metodo de borrar impuesto
            /// </summary>
            [TestMethod]
            public void RemoveTest()
            {
                Assert.IsTrue(true);
            }

            public void FixEfProviderServicesProblem()
            {
                var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
            }
        }
    }
}
