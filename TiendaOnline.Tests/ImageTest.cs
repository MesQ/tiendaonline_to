﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TiendaOnline.DAL;
using TiendaOnline.Application;
using TiendaOnline.CORE;

namespace TiendaOnline.Tests
{
    /// <summary>
    /// Summary description for ImageTest
    /// </summary>
    [TestClass]
    public class ImageTest
    {
        /// <summary>
        /// Metodo de añadir una imagen
        /// </summary>
        [TestMethod]
        public void AddTest()
        {
            //Arrange            
            ApplicationDbContext context = new ApplicationDbContext();
            ImagesManager manager = new ImagesManager(context);
             ProductImages img = new ProductImages { Id= 10000, PathFile = "Hola", Product_Id=1  };

            //Act
            manager.Add(img);

            //Assert
            Assert.AreEqual(manager.GetById(10000), img);
        }

        /// <summary>
        /// Metodo de borrar imagen
        /// </summary>
        [TestMethod]
        public void RemoveTest()
        {
            Assert.IsTrue(true);
        }

        public void FixEfProviderServicesProblem()
        {
            //The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            //for the 'System.Data.SqlClient' ADO.NET provider could not be loaded. 
            //Make sure the provider assembly is available to the running application. 
            //See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.

            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }
}
