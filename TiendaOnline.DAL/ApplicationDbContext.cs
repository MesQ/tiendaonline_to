﻿using TiendaOnline.CORE;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace TiendaOnline.DAL
{

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        /// <summary>
        /// Construcotr de aplicationdbcontext
        /// </summary>
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        /// <summary>
        /// crea el contexto de datos
        /// </summary>
        /// <returns></returns>
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        /// <summary>
        /// Genera tabla productos
        /// </summary>
        public DbSet<Product> Products { get; set; }

        /// <summary>
        /// Genera tabla categorias
        /// </summary>
        public DbSet<Category> Categories { get; set; }

        /// <summary>
        /// Genera tabla de perfiles
        /// </summary>
        public DbSet<Profile> Profiles { get; set; }

        /// <summary>
        /// Genera tabla de documentos
        /// </summary>
        public DbSet<Document> Documents { get; set; }

        /// <summary>
        /// Genera tabla de Pedidos
        /// </summary>
        public DbSet<Order> Orders { get; set; }

        /// <summary>
        /// Genera tabla de productos de pedidos
        /// </summary>
        public DbSet<OrderProduct> OrderProducts { get; set; }

        /// <summary>
        /// Genera tabla de carrito de compra
        /// </summary>
        public DbSet<Cart> Carts { get; set; }

        /// <summary>
        /// Genera tabla de impuestos
        /// </summary>
        public DbSet<Tax> Taxs { get; set; }

        /// <summary>
        /// Genera tabla de Imagenes de productos
        /// </summary>
        public DbSet<ProductImages> ProductImages { get; set; }
    }



}

