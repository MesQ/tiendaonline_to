﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TiendaOnline.CORE;
using TiendaOnline.DAL;

namespace TiendaOnline.Application
{
    /// <summary>
    /// Manager de documentos
    /// </summary>
    public class DocumentManager : GenericManager<Document>
    {
        /// <summary>
        /// Contructor de DocumentManager
        /// </summary>
        /// <param name="context">contexto de datos</param>
        public DocumentManager(ApplicationDbContext context) : base(context)
        {
        }
    }
}
