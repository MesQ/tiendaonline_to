﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TiendaOnline.CORE;
using TiendaOnline.DAL;

namespace TiendaOnline.Application
{
    /// <summary>
    /// manager del carrito
    /// </summary>
    public class CartManager : GenericManager<Cart>
    {

        public const string CartSessionKey = "CartId";

        /// <summary>
        /// constructor de CartManager
        /// </summary>
        /// <param name="context">contexto de datos</param>
        public CartManager(ApplicationDbContext context) : base(context)
        {
        }

        /// <summary>
        /// Metodo que obtiene la session de un usuario para el carrito
        /// </summary>
        /// <returns></returns>
        public string GetCartId()
        {
            if (HttpContext.Current.Session[CartSessionKey] == null)
            {
                if (!string.IsNullOrWhiteSpace(HttpContext.Current.User.Identity.Name))
                {
                    HttpContext.Current.Session[CartSessionKey] = HttpContext.Current.User.Identity.Name;
                }
                else
                {
                    // Generate a new random GUID using System.Guid class.     
                    Guid tempCartId = Guid.NewGuid();
                    HttpContext.Current.Session[CartSessionKey] = tempCartId.ToString();
                }
            }
            return HttpContext.Current.Session[CartSessionKey].ToString();
        }

        /// <summary>
        /// Metodo que obtiene todos los productos 
        /// del carrito de un usuario
        /// </summary>
        /// <returns>productos</returns>
        public List<TiendaOnline.CORE.Cart> GetCartItems()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            CartManager manager = new CartManager(context);
            string session = manager.GetCartId();
            var query = manager.Context.Carts;
            return query.Where(c => c.SessionId == session).ToList();
        }

        /// <summary>
        /// Metodo que obtiene el precio total
        /// de todos los productos del carrito
        /// </summary>
        /// <returns>precio total</returns>
        public decimal GetTotal()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            CartManager manager = new CartManager(context);
            string CartId = manager.GetCartId();

            decimal? total = decimal.Zero;
            total = (decimal?)(from Carts in manager.Context.Carts
                               where Carts.SessionId == CartId
                               select (int?)Carts.Quantity *
                               Carts.Product.Price).Sum();
            return total ?? decimal.Zero;
        }


        /// <summary>
        /// Metodo que obtiene los impuestos del precio total
        /// </summary>
        /// <returns>precio impuestos</returns>
        public decimal GetTaxes()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            CartManager manager = new CartManager(context);

            decimal total = GetTotal();

            total = (total * 21) / 100;

            return total;
        }

        /// <summary>
        /// Metodo que borra todos los productos 
        /// del carrito de un usuario
        /// </summary>
        public void EmptyCart()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            CartManager cartManager = new CartManager(context);
                string cartId = cartManager.GetCartId();
                var query = cartManager.Context.Carts;
                query.Where(c => c.SessionId == cartId);

                foreach (var cartItem in query)
                {
                    cartManager.Remove(cartItem);
                }

                cartManager.Context.SaveChanges();
            }
        }

    }

