﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TiendaOnline.CORE;
using TiendaOnline.DAL;

namespace TiendaOnline.Application
{
    /// <summary>
    /// Manager de OrderProduct
    /// </summary>
    public class OrderProductManager : GenericManager<OrderProduct>
    {
        /// <summary>
        /// Constructor del manager orderprooduct
        /// </summary>
        /// <param name="context"></param>
        public OrderProductManager(ApplicationDbContext context) : base(context)
        {
        }
    }
}
