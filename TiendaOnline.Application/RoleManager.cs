﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TiendaOnline.CORE;
using TiendaOnline.DAL;

namespace TiendaOnline.Application
{
    public class RoleManager
    {
        /// <summary>
        /// Aañade rol a usuario
        /// </summary>
        public void AddUserAndRole()
        {
            
            ApplicationDbContext context = new ApplicationDbContext();
            IdentityResult IdRoleResult;
            IdentityResult IdUserResult;

            var roleStore = new RoleStore<IdentityRole>(context);

            var roleMgr = new RoleManager<IdentityRole>(roleStore);

            // Crea el rol admin y registered si no existen
            if (!roleMgr.RoleExists("Admin"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "Admin" });
            }

            if (!roleMgr.RoleExists("Registered"))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = "Registered" });
            }
            // crea el usuario admin
            var userMgr = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var appUser = new ApplicationUser
            {
                UserName = "admin",
                Email = "admin@tiendaonline.com"
            };
            IdUserResult = userMgr.Create(appUser, "1A2B3C4d");

            // si el usuario admin es creado lo añade al rol admin
            if (!userMgr.IsInRole(userMgr.FindByEmail("admin@tiendaonline.com").Id, "Admin"))
            {
                IdUserResult = userMgr.AddToRole(userMgr.FindByEmail("admin@tiendaonline.com").Id, "Admin");
            }
        }
    }
}

