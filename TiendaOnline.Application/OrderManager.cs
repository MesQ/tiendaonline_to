﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TiendaOnline.CORE;
using TiendaOnline.DAL;

namespace TiendaOnline.Application
{
    /// <summary>
    /// Manager de pedidos
    /// </summary>
    public class OrderManager : GenericManager<Order>
    {
        /// <summary>
        /// Constructor de OrderManager
        /// </summary>
        /// <param name="context">contexto de datos</param>
        public OrderManager(ApplicationDbContext context) : base(context)
        {
        }

        /// <summary>
        /// Método que retorna todos los pedidos de un usuario
        /// </summary>
        /// <param name="userId">Identificador de usuario</param>
        /// <returns>Todos los pedidos del usuario</returns>
        public IQueryable<Order> GetByUserId(string userId)
        {
            return Context.Set<Order>().Where(e => e.Client_Id == userId);
        }
    }
}
