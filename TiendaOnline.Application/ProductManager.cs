﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TiendaOnline.CORE;
using TiendaOnline.DAL;

namespace TiendaOnline.Application
{
    /// <summary>
    /// Manager de Product
    /// </summary>
    public class ProductManager : GenericManager<Product>
    {
        /// <summary>
        /// Constructor del manager de product
        /// </summary>
        /// <param name="context">contexto de datos</param>
        public ProductManager(ApplicationDbContext context) : base(context)
        {
        }

        /// <summary>
        /// Metodo que obtiene los productos por su categoria
        /// </summary>
        /// <param name="id">id de categoria</param>
        /// <returns></returns>
        public IQueryable<Product> GetByCategory(int id)
        {
            return Context.Set<Product>().Where(e => e.Category_Id == id);
        }
    }
}
