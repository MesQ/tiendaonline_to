﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TiendaOnline.CORE;
using TiendaOnline.DAL;

namespace TiendaOnline.Application
{
    /// <summary>
    /// Manager de Profiles
    /// </summary>
    public class ProfileManager : GenericManager<Profile>
    {
        /// <summary>
        /// Constructor de ProfileManger
        /// </summary>
        /// <param name="context">contexto de datos</param>
        public ProfileManager(ApplicationDbContext context) : base(context)
        {
        }

        /// <summary>
        /// Obtiene una entidad por el Client_Id
        /// </summary>
        /// <param name="client">id de cliente</param>
        /// <returns>entidad</returns>
        public int GetByClientId(string client)
        {
            IQueryable<Profile> query = Context.Profiles;
            int id = query.Where(p => p.Client_Id == client).Select(a => a.Id).FirstOrDefault(); 
            return id;
        }
    }
}
