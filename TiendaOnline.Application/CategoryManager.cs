﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TiendaOnline.CORE;
using TiendaOnline.DAL;

namespace TiendaOnline.Application
{
    /// <summary>
    /// Manager de categoria
    /// </summary>
    public class CategoryManager : GenericManager<Category>
    {
        /// <summary>
        /// Constructor de CategoryManager
        /// </summary>
        /// <param name="context">contexto de datos</param>
        public CategoryManager(ApplicationDbContext context) : base(context)
        {
        }
    }
}
