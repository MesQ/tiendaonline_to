﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TiendaOnline.CORE;
using TiendaOnline.DAL;

namespace TiendaOnline.Application
{
    /// <summary>
    /// manager de usuarios
    /// </summary>
    public class UserManager : GenericManager<ApplicationUser>
    {
        /// <summary>
        /// Constructor de user manager
        /// </summary>
        /// <param name="context"></param>
        public UserManager(ApplicationDbContext context) : base(context)
        {
        }

        /// <summary>
        /// Obtiene una entidad por id del usuario
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>entidad</returns>
        public ApplicationUser GetByUserId(string id)
        {
            return Context.Set<ApplicationUser>().Find(id);
        }
    }
}
