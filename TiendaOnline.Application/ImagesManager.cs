﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TiendaOnline.CORE;
using TiendaOnline.DAL;

namespace TiendaOnline.Application
{
    /// <summary>
    /// manager de imagenes de producto
    /// </summary>
    public class ImagesManager : GenericManager<ProductImages>
    {
        /// <summary>
        /// constructor de ImagesManager
        /// </summary>
        /// <param name="context">contexto de datos</param>
        public ImagesManager(ApplicationDbContext context) : base(context)
        {
        }
    }
}
