﻿using System;
using System.Data.Entity.Migrations;
using System.Linq;
using System.IO;
using TiendaOnline.DAL;

namespace TiendaOnline.Application
{
    /// <summary>
    /// Manager generico
    /// </summary>
    public class GenericManager<T>
        where T : class
    {
        /// <summary>
        /// Contexto de datos
        /// </summary>
        public ApplicationDbContext Context { get; private set; }

        /// <summary>
        /// Contructor del manager
        /// </summary>
        /// <param name="context">contexto de datos</param>
        public GenericManager(ApplicationDbContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Añade una entidad
        /// </summary>
        /// <param name="entity">entidad</param>
        /// <returns>Entidad añadida</returns>
        public T Add(T entity)
        {
            return Context.Set<T>().Add(entity);
        }

        /// <summary>
        /// Aactualiza una entidad
        /// </summary>
        /// <param name="entity">entidad</param>
        /// <returns>Entidad actualizada</returns>
        public T Update(T entity)
        {
            //Context.Set<T>().Attach(entity);
            //Context.Entry(entity).State = EntityState.Modified;
            Context.Set<T>().AddOrUpdate<T>(entity);
            return entity;
        }

        /// <summary>
        /// Elimina una entidad
        /// </summary>
        /// <param name="entity">entidad</param>
        /// <returns>Entidad eliminada</returns>
        public T Remove(T entity)
        {
            return Context.Set<T>().Remove(entity);
        }

        /// <summary>
        /// Busca entidad por su clave
        /// </summary>
        /// <param name="key">entidad</param>
        /// <returns>entidad encontrada</returns>
        public T Find(object[] key)
        {
            return Context.Set<T>().Find(key);
        }

        /// <summary>
        /// Busca entidad por int
        /// </summary>
        /// <param name="key"></param>
        /// <returns>entidad encontrada</returns>
        public T Find(int key)
        {
            return Context.Set<T>().Find(new object[] { key });
        }

        /// <summary>
        /// Obtiene una entidad por sus claves
        /// </summary>
        /// <param name="key">clave objeto</param>
        /// <returns>entidad encontrada</returns>
        public T GetById(object[] key)
        {
            return Context.Set<T>().Find(key);
        }

        /// <summary>
        /// Obtiene una entidad por su id int
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>entidad</returns>
        public T GetById(int id)
        {
            return GetById(new object[] { id });
        }

        /// <summary>
        /// Obtiene todas las entidades de un tipo
        /// </summary>
        /// <returns>lista todo</returns>
        public IQueryable<T> GetAll()
        {
            return Context.Set<T>();
        }

        /// <summary>
        /// Metodo que guarda los errores en un archivo log
        /// </summary>
        /// <param name="sPathName">ruta archivo</param>
        /// <param name="sErrMsg">mensaje de error</param>
        public void ErrorLog(string sPathName, string sErrMsg)
        {

            string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";

            string sYear = DateTime.Now.Year.ToString();
            string sMonth = DateTime.Now.Month.ToString();
            string sDay = DateTime.Now.Day.ToString();
            string sErrorTime = sYear + sMonth + sDay;

            StreamWriter sw = new StreamWriter(sPathName, true);
            sw.WriteLine(sLogFormat + sErrMsg);
            sw.Flush();
            sw.Close();
        }

        /// <summary>
        /// Busca entidad por nombre
        /// </summary>
        /// <param name="key"></param>
        /// <returns>entidad encontrada</returns>
        public T FindByName(string key)
        {
            return Context.Set<T>().Find(new object[] { key });
        }



    }
}
