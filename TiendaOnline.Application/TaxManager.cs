﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TiendaOnline.CORE;
using TiendaOnline.DAL;

namespace TiendaOnline.Application
{
    /// <summary>
    /// Manager de impuestos
    /// </summary>
    public class TaxManager : GenericManager<Tax>
    {
        /// <summary>
        /// Constructor de TaxManager
        /// </summary>
        /// <param name="context">contexto de datos</param>
        public TaxManager(ApplicationDbContext context) : base(context)
        {
        }
    }
}
