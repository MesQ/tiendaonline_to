﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TiendaOnline.CORE
{
    /// <summary>
    /// Entidad de dominio categoria
    /// </summary>
    public class Category
    {
        /// <summary>
        /// Id categoria
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre categoría
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descripcion categoria
        /// </summary>
        public string Description { get; set; }

    }
}
