﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiendaOnline.CORE
{
    /// <summary>
    /// Estados del pedido
    /// </summary>
    public enum OrderStatus : int
    {
        /// <summary>
        /// El pedido ha sido recibido
        /// </summary>
        Nuevo = 1,

        /// <summary>
        /// El pedido ha sido recibido
        /// </summary>
        Recibido = 2,

        /// <summary>
        /// El pedido se ha pagado
        /// </summary>
        Pagado = 3,

        /// <summary>
        /// El pedido se está preparando
        /// </summary>
        EnProgreso = 4,

        /// <summary>
        /// El pedido ha sido enviado
        /// </summary>
        Enviado = 5,

        /// <summary>
        /// El pedido ha sido entregado
        /// </summary>
        Entregado = 6
    }
}
