﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiendaOnline.CORE
{
    public enum PaymentMethod : int
    {
        /// <summary>
        /// El metodo de pago es en efectivo
        /// </summary>
        Efectivo = 1,

        /// <summary>
        /// El metodo de pago es con tarjeta de credito
        /// </summary>
        Tarjeta = 2,

        /// <summary>
        /// El metodo de pago es con PayPal
        /// </summary>
        PayPal=3,

        /// <summary>
        /// El metodo de pago con transferencia bancaria
        /// </summary>
        Transferencia=4,

    }
}
