﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiendaOnline.CORE
{
    /// <summary>
    /// Entidad de dominio Pedido
    /// </summary>
    public class Order
    {
        /// <summary>
        /// Identificador del pedido
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Id del usuario que ha hecho el pedido
        /// </summary>
        [ForeignKey("Client")]
        [Required]
        public string Client_Id { get; set; }

        /// <summary>
        /// Usuario del pedido
        /// </summary>
        public virtual ApplicationUser Client { get; set; }

        /// <summary>
        /// Estado del pedido
        /// </summary>
        public OrderStatus Status { get; set; }

        /// <summary>
        /// Fecha hora compra
        /// </summary>
        public DateTime PurchaseDate { get; set; }

        /// <summary>
        /// Fecha hora envio
        /// </summary>
        public DateTime? SendDate { get; set; }

        /// <summary>
        /// Pagado si o no
        /// </summary>
        public bool Paid { get; set; }

        /// <summary>
        /// Método de pago
        /// </summary>
        public PaymentMethod PaymentMethod { get; set; }

        /// <summary>
        /// Pedido enviado si o no
        /// </summary>
        public bool Shipped { get; set; }

        /// <summary>
        /// Seguimiento envio Empresa y numero seguimiento
        /// </summary>
        public string Tracking { get; set; }

        /// <summary>
        /// Dirección de envío
        /// </summary>
        public virtual Profile Profile { get; set; }

        /// <summary>
        /// Identificador de direccion de envio
        /// </summary>
        [ForeignKey("Profile")]
        public int Profile_Id { get; set; }

        /// <summary>
        /// Precio total del pedido
        /// </summary>
        public decimal TotalPrice { get; set; }

    }
}
