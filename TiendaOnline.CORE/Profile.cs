﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TiendaOnline.CORE
{
    /// <summary>
    /// Entidad de dominio de perfil
    /// </summary>
    public class Profile
    {
        /// <summary>
        /// Id perfil
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre de la persona
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// DNI de la persona
        /// </summary>
        public string DNI { get; set; }

        /// <summary>
        /// Teléfono de contacto
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Pais
        /// </summary>        
        public string Country { get; set; }

        /// <summary>
        /// Provincia
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Calle
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Datos calle, número, portal, puerta...
        /// </summary>
        public string StreetDetail { get; set; }

        /// <summary>
        /// Dirección de envío
        /// </summary>
        public virtual ApplicationUser Client { get; set; }

        /// <summary>
        /// Identificador de direccion de envio
        /// </summary>
        [ForeignKey("Client")]
        public string Client_Id
        {
            get; set;
        }
    }
}
