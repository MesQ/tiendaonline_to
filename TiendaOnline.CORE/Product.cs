﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TiendaOnline.CORE
{
    /// <summary>
    /// Entidad de dominio de producto
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Id producto
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descripción del producto
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Precio del producto
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Categoria del producto
        /// </summary>
        public virtual Category Category { get; set; }

        /// <summary>
        /// Id de categoria
        /// </summary>
        [ForeignKey("Category")]
        public int Category_Id { get; set; }

        /// <summary>
        /// Impuesto del producto
        /// </summary>
        public virtual Tax Tax { get; set; }

        /// <summary>
        /// Id del impuesto
        /// </summary>
        [ForeignKey("Tax")]
        public int Tax_Id { get; set; }

        /// <summary>
        /// Stock producto
        /// </summary>
        public int Stock { get; set; }

        /// <summary>
        ///  Imagen Principal del producto
        /// </summary>
        public string PathFile { get; set; }

    }
}
