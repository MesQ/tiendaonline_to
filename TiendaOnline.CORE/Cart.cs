﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiendaOnline.CORE
{
    /// <summary>
    /// Clase de dominio de carrito
    /// </summary>
    public class Cart
    {
        /// <summary>
        /// Identificador del carrito
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identificador de la sesión
        /// </summary>
        public string SessionId { get; set; }

        /// <summary>
        /// Usuario actual dueño del carrito
        /// </summary>
        public ApplicationUser Client { get; set; }

        /// <summary>
        /// Identificador del Usuario actual dueño del carrito
        /// </summary>
        [ForeignKey("Client")]
        public string Client_Id { get; set; }

        /// <summary>
        /// Producto añadido 
        /// </summary>
        public virtual Product Product { get; set; }

        /// <summary>
        /// Id del producto añadido al carrito
        /// </summary>
        [ForeignKey("Product")]
        public int Product_Id { get; set; }

        /// <summary>
        /// Cantidad añadida
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Fecha  de cuando se ha añadido el producto
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Precio  del producto al añadirlo a la cesta
        /// </summary>
        public decimal Price { get;set;}
    }
}
