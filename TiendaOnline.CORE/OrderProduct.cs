﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TiendaOnline.CORE
{
    /// <summary>
    /// Entidad de dominio de productos del pedido
    /// </summary>
    public class OrderProduct
    {
        /// <summary>
        /// Id 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Producto 
        /// </summary>
        public virtual Order Order { get; set; }

        /// <summary>
        /// Id de pedido
        /// </summary>
        [ForeignKey("Order")]
        public int Order_Id { get; set; }

        /// <summary>
        /// Producto 
        /// </summary>
        public virtual Product Product { get; set; }

        /// <summary>
        /// Id producto
        /// </summary>
        [ForeignKey("Product")]
        public int Product_Id { get; set; }

        /// <summary>
        /// Cantidad 
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Precio
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Precio total incluidos impuestos
        /// </summary>
        public decimal Total { get; set; }        
    }
}
