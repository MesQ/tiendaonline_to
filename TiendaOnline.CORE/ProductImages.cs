﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiendaOnline.CORE
{
    /// <summary>
    /// Entidad de dominio de imagenes de productos
    /// </summary>
    public class ProductImages
    {

        /// <summary>
        /// Id imagen
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Ruta de la imagen
        /// </summary>
        public string PathFile { get; set; }

        /// <summary>
        /// Producto de la imagen
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// Identificador de direccion de envio
        /// </summary>
        [ForeignKey("Product")]
        public int Product_Id { get; set; }
    }
}
