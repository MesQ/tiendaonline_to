﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TiendaOnline.CORE
{
    /// <summary>
    /// Entidad de dominio Documentos
    /// </summary>
    public class Document
    {
        /// <summary>
        /// Id documento
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre documento
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descripcion del documento
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Producto al que pertenece el documento
        /// </summary>
        public virtual Product Product { get; set; }

        /// <summary>
        /// Identificador del producto al que pertenece
        /// </summary>
        [ForeignKey("Product")]
        public int Product_Id { get; set; }

        /// <summary>
        /// Ruta del archivo
        /// </summary>
        public string PathFile { get; set; }
    }
}
