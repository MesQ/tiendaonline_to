﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiendaOnline.CORE
{
    /// <summary>
    /// Entidad de dominio impuestos
    /// </summary>
    public class Tax
    {
        /// <summary>
        /// Id impuesto
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre impuesto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Porcentaje del impuesto
        /// </summary>
        public decimal Percentage { get; set; }

    }
}
